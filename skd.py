from os.path import join
from datetime import datetime

from fabric.api import cd, env, run, task, prefix
from fabric.colors import red


@task
def test():
    env.hosts = ['shc.steelkiwi.com:2216']
    env.project_name = 'project'
    env.user = 'webmaster'
    env.base_dir = join('/', 'home', env.user)
    env.project_path = join(env.base_dir, env.project_name)
    env.venv = join(env.base_dir, 'env')
    env.branch = 't47027_Redesign'
    env.django_settings = 'cms_app.settings'
    env.sv_group = 'shc'
    env.server_type = 'staging'


env.use_ssh_config = True


def activate_venv():
    return ('source %(venv)s/bin/activate' % env)


def django_settings():
    return ('export DJANGO_SETTINGS_MODULE=%(django_settings)s' % env)


@task
def manage(command):
    with cd(env.project_path):
        with prefix(activate_venv()):
            with prefix(django_settings()):
                run('python manage.py {}'.format(command))


@task
def update():
    with cd(env.project_path):
        run('git pull origin {}'.format(env.branch))
        # install_requirements()
        # delete_pyc_files()
        # apply_migrations()
        # collect_static_files()
        # load_fixtures()
        # restart()


@task
def install_requirements():
    with cd(env.project_path):
        with prefix(activate_venv()):
            run('pip install -r requirements.txt')


@task
def collect_static_files(noinput=True, verbosity='0'):
    manage('collectstatic {} --verbosity={}'.format(('--noinput' if noinput else ''), verbosity))


@task
def apply_migrations():
    manage('migrate')


@task
def delete_pyc_files():
    with cd(env.project_path):
        run('find . -name "*.pyc" -exec rm -f {} \;')


@task
def restart(all=True):
    run('supervisorctl restart {}:'.format(env.sv_group))


@task
def status():
    run('supervisorctl status')

