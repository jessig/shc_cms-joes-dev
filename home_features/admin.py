from django.contrib import admin
from .models import HomeFeature

class HomeFeatureAdmin(admin.ModelAdmin):
	list_display = ('title', 'link_to', 'section', 'active', 'order')
	list_editable = ('active', 'order')
	list_filter = ('active', 'section')
	ordering = ('section', '-active', 'order')

# Register your models here.
admin.site.register(HomeFeature, HomeFeatureAdmin)
