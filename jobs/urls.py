from django.conf.urls import patterns, include, url

urlpatterns = patterns('jobs.views',
    url(r'^$', 'entry_list', name='entry_list'),
    url(r'^(?P<id>[0-9]+)/$', 'entry_detail', name='entry_detail'),
)