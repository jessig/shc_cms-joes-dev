from django.db import models
from feincms.content.application import models as app_models
from django.db.models import Q
from ckeditor.fields import RichTextField
from datetime import datetime

JOB_TYPE_CHOICES = (
	('fac','Faculty'),
	('sta','Staff')
	)

class ActiveJobPostingManager(models.Manager):
    def get_query_set(self):
        return super(ActiveJobPostingManager, self).get_query_set().filter(
        	Q(post_date__lte=datetime.now()),
        	Q(last_date__gte=datetime.now()) | Q(last_date__isnull=True)
        	)

class JobPosting(models.Model):
	title = models.CharField(
		max_length=100
		)
	type = models.CharField(
		max_length=5,
		choices=JOB_TYPE_CHOICES
		)
	summary = RichTextField(
		)
	job_description = models.FileField(
		upload_to = "jobs/%Y/%m/%d",
		blank=True,
		null=True
		)
	post_date = models.DateTimeField(
		default=datetime.now,
		help_text="Postings are sorted reverse chronologically by post date"
		)
	last_date = models.DateTimeField(
		blank=True,
		null=True
		)
	posted_date = models.DateTimeField(
		auto_now_add=True
		)
	last_updated = models.DateTimeField(
		auto_now=True
		)

	all_jobs = models.Manager()
	objects = ActiveJobPostingManager()

	def __unicode__(self):
		return self.title

	class Meta:
		ordering = ['-post_date']

	# @app_models.permalink
	# def get_absolute_url(self):
	# 	return ('entry_detail', 'news.urls', (), {
	# 		'id': self.id,
	# 	})
