# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='JobPosting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('type', models.CharField(max_length=5, choices=[(b'fac', b'Faculty'), (b'sta', b'Staff')])),
                ('summary', ckeditor.fields.RichTextField()),
                ('job_description', models.FileField(null=True, upload_to=b'jobs/%Y/%m/%d', blank=True)),
                ('post_date', models.DateTimeField(default=datetime.datetime(2017, 4, 20, 11, 25, 0, 190552), help_text=b'Postings are sorted reverse chronologically by post date')),
                ('last_date', models.DateTimeField(null=True, blank=True)),
                ('posted_date', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['-post_date'],
            },
            bases=(models.Model,),
        ),
    ]
