# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobposting',
            name='post_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 5, 9, 13, 51, 11, 382551), help_text=b'Postings are sorted reverse chronologically by post date'),
            preserve_default=True,
        ),
    ]
