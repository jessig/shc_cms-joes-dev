# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0004_auto_20170426_1319'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profilemediafilecontent',
            name='type',
            field=models.CharField(default='default', max_length=20, verbose_name='type', choices=[('default', 'default'), ('banner', 'banner'), ('captioned banner', 'banner w/ caption'), ('inline', 'inline'), ('captioned inline', 'inline w/ caption'), ('responsive', 'responsive'), ('captioned responsive', 'responsive w/ caption'), ('headshot', 'headshot'), ('captioned headshot', 'headshot w/ caption')]),
            preserve_default=True,
        ),
    ]
