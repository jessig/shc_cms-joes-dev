# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import feincms.module.mixins
import feincms.extensions
import feincms.contrib.richtext
import feincms.module.medialibrary.fields


class Migration(migrations.Migration):

    dependencies = [
        ('medialibrary', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('description', models.CharField(max_length=200, verbose_name='description', blank=True)),
                ('ordering', models.PositiveIntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ('ordering', 'name'),
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True, db_index=True, verbose_name='is active')),
                ('is_featured', models.BooleanField(default=False, db_index=True, verbose_name='is featured')),
                ('title', models.CharField(max_length=100, verbose_name='title')),
                ('blurb', models.CharField(max_length=250, verbose_name='blurb')),
                ('slug', models.SlugField(unique=True, max_length=100, verbose_name='slug')),
                ('last_changed', models.DateTimeField(auto_now=True, verbose_name='last change')),
                ('_content_title', models.TextField(help_text='The first line is the main title, the following lines are subtitles.', verbose_name='content title', blank=True)),
                ('_page_title', models.CharField(help_text='Page title for browser window. Same as title by default. Must be 69 characters or fewer.', max_length=69, verbose_name='page title', blank=True)),
                ('categories', models.ManyToManyField(related_name='blogentries', verbose_name='categories', to='profiles.Category', blank=True)),
                ('image', feincms.module.medialibrary.fields.MediaFileForeignKey(related_name='+', blank=True, to='medialibrary.MediaFile', null=True)),
            ],
            options={
                'verbose_name': 'profile',
                'verbose_name_plural': 'profiles',
            },
            bases=(models.Model, feincms.extensions.ExtensionsMixin, feincms.module.mixins.ContentModelMixin),
        ),
        migrations.CreateModel(
            name='ProfileMediaFileContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('type', models.CharField(default='default', max_length=20, verbose_name='type', choices=[('default', 'default'), ('banner', 'banner'), ('captioned banner', 'banner w/ caption'), ('inline', 'inline'), ('captioned inline', 'inline w/ caption'), ('responsive', 'responsive'), ('captioned responsive', 'responsive w/ caption')])),
                ('mediafile', feincms.module.medialibrary.fields.MediaFileForeignKey(related_name='+', verbose_name='media file', to='medialibrary.MediaFile')),
                ('parent', models.ForeignKey(related_name='profilemediafilecontent_set', to='profiles.Profile')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'media files',
                'db_table': 'profiles_profile_profilemediafilecontent',
                'verbose_name': 'media file',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProfileRawContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(verbose_name='content', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='profilerawcontent_set', to='profiles.Profile')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'raw contents',
                'db_table': 'profiles_profile_profilerawcontent',
                'verbose_name': 'raw content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RichTextProfileContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', feincms.contrib.richtext.RichTextField(verbose_name='text', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='richtextprofilecontent_set', to='profiles.Profile')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'rich texts',
                'db_table': 'profiles_profile_richtextprofilecontent',
                'verbose_name': 'rich text',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TextileProfileContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('show_on', models.DateTimeField(help_text='leave blank to publish now', null=True, blank=True)),
                ('hide_after', models.DateTimeField(help_text='leave blank for no expiration', null=True, blank=True)),
                ('content', models.TextField()),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='textileprofilecontent_set', to='profiles.Profile')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'textile page contents',
                'db_table': 'profiles_profile_textileprofilecontent',
                'verbose_name': 'textile page content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
    ]
