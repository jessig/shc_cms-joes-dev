from django.conf.urls import url, include, patterns
from profiles.views import profile_content

urlpatterns = patterns('',
                       url(r'^content/(?P<pid>\d+)$',
                           profile_content, name='profile_content'),
                       url(r'^(?P<slug>[-\w]+)/$',
                           profile_content, name='profile'),
                       )
