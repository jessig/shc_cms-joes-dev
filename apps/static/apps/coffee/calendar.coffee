qs = (obj) ->
    obj = {src:obj.google_cal_id, color:obj.color, $$hashKey:obj.$$hashKey}
    str = (k + "=" + encodeURIComponent(v) for k, v of obj)
    str.join("&");

calApp = angular.module('SHCCalendar', []).config ($compileProvider) ->

calApp.filter 'encodeURIComponent', () ->
    window.encodeURIComponent

calApp.controller('CalendarCtrl', ($scope) ->
    $scope.calendar = {}
    $scope.calendar.calendars = [
        {
            id: "academic"
            title: "Academic Calendar"
            google_cal_id:"shc.edu_bgkokkn4tjul1v296vdeo9mem8@group.calendar.google.com"
            color:"#668CB3"
            ical: "https://www.google.com/calendar/ical/shc.edu_bgkokkn4tjul1v296vdeo9mem8%40group.calendar.google.com/public/basic.ics"
        }
        {
            id: "academicevents"
            title: "Academic Events"
            google_cal_id:"shc.edu_d1bq3b4itk9mmhhctjotd5mp1g@group.calendar.google.com"
            color:"#5A6986"
            ical: "https://www.google.com/calendar/ical/shc.edu_d1bq3b4itk9mmhhctjotd5mp1g%40group.calendar.google.com/public/basic.ics"
        }
        {
            id: "admissions"
            title: "Admissions Events"
            google_cal_id:"shc.edu_kaccrui6t524gu79gs0k7g1pjs@group.calendar.google.com"
            color:"#4CB052"
            ical: "https://www.google.com/calendar/ical/shc.edu_kaccrui6t524gu79gs0k7g1pjs%40group.calendar.google.com/public/basic.ics"
        }
        {
            id: "alumni"
            title: "Alumni Events"
            google_cal_id:"shc.edu_2vlsr4ubl29erjpucim5lotdkk@group.calendar.google.com"
            color:"#CF9911"
            ical: "https://www.google.com/calendar/ical/shc.edu_2vlsr4ubl29erjpucim5lotdkk%40group.calendar.google.com/public/basic.ics"
        }
        {
            id: "athletics"
            title: "Athletic Events"
            google_cal_id:"shc.edu_ft51vl6mn8iiqe1mesir50af94@group.calendar.google.com"
            color:"#23164E"
            ical: "https://www.google.com/calendar/ical/shc.edu_ft51vl6mn8iiqe1mesir50af94%40group.calendar.google.com/public/basic.ics"
        }
        {
            id: "conferences"
            title: "Conferences"
            google_cal_id:"shc.edu_8gqdd7afi5ctku99gss1kkca50@group.calendar.google.com"
            color:"#865A5A"
            ical: "https://www.google.com/calendar/ical/shc.edu_8gqdd7afi5ctku99gss1kkca50%40group.calendar.google.com/public/basic.ics"
        }
        {
            id: "finearts"
            title: "Fine Arts Events"
            google_cal_id:"shc.edu_752uapm7dsi466951a6k0b2558@group.calendar.google.com"
            color:"#2F6309"
            ical: "https://www.google.com/calendar/ical/shc.edu_752uapm7dsi466951a6k0b2558%40group.calendar.google.com/public/basic.ics"
        }
        {
            id: "ministry"
            title: "Ministry and Spirituality"
            google_cal_id:"shc.edu_0tsm8tmgisepq1df0dcos9k7ik@group.calendar.google.com"
            color:"#AD2D2D"
            ical: "https://www.google.com/calendar/ical/shc.edu_0tsm8tmgisepq1df0dcos9k7ik%40group.calendar.google.com/public/basic.ics"
        }
        {
            id: "student"
            title: "Student Events"
            google_cal_id:"shc.edu_sq0v8fvdrin2u4l83k4thoo41k@group.calendar.google.com"
            color:"#C7561E"
            ical: "https://www.google.com/calendar/ical/shc.edu_sq0v8fvdrin2u4l83k4thoo41k%40group.calendar.google.com/public/basic.ics"
        }
        # {id: "public", title: "Public Events", google_cal_id:"shc.edu_n4hqd8mcbqvljeqebh917jshrk@group.calendar.google.com", color:"#865A5A"}
    ]

    $scope.calendar.get_cal_embed = () ->
        (qs(cal) for cal in $scope.calendar.calendars).join("&")
)