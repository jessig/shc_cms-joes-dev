# checks the loading state of an iframe and turns off the spinner once loaded
$ ->
	$("iframe").on 'load', () ->
		$(this).closest('.iframe-container').css('background-image', 'none')