Number.prototype.formatMoney = (c, d, t) ->
	n = this
	c = if isNaN(c = Math.abs(c)) then 2 else c
	d = if not d? then "." else d
	t = if not t? then "," else t
	s = if n < 0 then "-" else ""
	i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + ""
	j = if (j = i.length) > 3 then j % 3 else 0
	return s + (if j then i.substr(0, j) + t else "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (if c then d + Math.abs(n - i).toFixed(c).slice(2) else "")

extend = (object, properties) ->
  for key, val of properties
    object[key] = val
  object

angular.module('SHCFinAidAfford', ['ngSanitize', 'LocalStorageModule', 'mm.foundation'])

	.controller 'AffordCtrl', ($scope, $location, localStorageService) ->

		local_storage_key = 'shc_afford'

		ui_strings = {
			alerts: {
				form_preloaded: 'This form was pre-loaded with information provided by the link you clicked.'
				has_stored_data: 'You have data stored from a previous session. Click the "Load data" button below to restore it.'
			}
			messages: {
				intro: """This tool is provided as an estimator to help you understand
				and calculate the cost of completing your Spring Hill College education.
				If you have your award package, please use the amounts listed for the
				financial aid contribution.  Please feel free to contact your admissions
				or financial aid counselor to review or answer any questions.
				"""
				disclaimer: """<p>This financial aid calculator provides an <em>estimate</em> of your
				four-year college costs based on our estimates of college costs and
				recent trends in student loan allocations. Spring Hill College
				makes no guarantees that this information will precisely reflect your total four
				year college costs.</p><p>Books are an additional cost and some courses require
				lab fees, which are indicated on the schedule of classes available at registration.</p>
				<p>The College reserves the right to change tuition, fees, services, or programs at any
				time without prior notification.</p>
				"""
				privacy: """This financial aid calculator does not transmit any of your data to
				Spring Hill College or any other entity. All calculations are made directly in your
				browser via Javascript and stored locally in your browser only when a you click the
				"save" button. All stored information can be deleted from the browser by
				clicking the "clear" button.
				"""
			}
		}

		$scope.user_data = {}
		$scope.user_data.data = {}

		$scope.user_data.student_type = {
			oncampus: {
				title: "Undergraduate living on campus"
				tuition_first_yr: 34091
				room_board_first_yr: 12226
			}
			offcampus: {
				title: "Undergraduate living off campus"
				tuition_first_yr: 34091
				room_board_first_yr: 0
			}
			grad: {
				title: "Graduate student"
				tuition_first_yr: 20799
				room_board_first_yr: 0
			}
		}

		$scope.user_data.data.selected_student_type = {amt: "", default: "oncampus"}

		# shortcut function
		$scope.sel_student_type = ->
			# console.log "student type: " + $scope.user_data.data.selected_student_type['amt']
			$scope.user_data.student_type[$scope.user_data.data.selected_student_type['amt']]


		$scope.factors = {
			tuition_perc_increase_yr: 5
			room_board_perc_increase_yr: 5
			# this is the yearly increase *above the first year* that
			#  federal financial aid increases.
			#  The first number should always be 0.
			fed_loan_yearly_increase: [0,1000,2000,2000]
		}

		default_decorators = {
			money: ['$', '00']
		}
		default_validators = {
			number: true
			min: '0'
			step: '100'
		}
		default_error_msg = {
			number: 'This is not a valid number'
			min: 'Dollar amounts must be greater than 0'
		}

		$scope.user_data.data = extend $scope.user_data.data,
			{
				'sg': {
					id: 'shc_schol_grants'
					title: 'Spring Hill Scholarships and Grants'
					help: """This includes academic merit, visit grant, Spring Hill
					grant, Legacy grant – any scholarship or grant funded by
					Spring Hill College"""
					default: ''
					type: 'fin_aid'
					decorators: default_decorators.money
					validators: default_validators
					error_msg: default_error_msg
					# validators: {
					# 	number: default_validators.number
					# 	max: $scope.sel_student_type().tuition_first_yr
					# }
					# error_msg: {
					# 	number: default_error_msg.number
					# 	max: "Aid cannot exceed tuition (#{$scope.sel_student_type().tuition_first_yr.formatMoney(0)})"
					# }
				}
				'fed': {
					id: 'fed_aid'
					title: 'Federal Aid'
					help: """Grant Aid only here – Pell Grant, SEOG, TEACH"""
					default: ''
					type: 'fin_aid'
					decorators: default_decorators.money
					validators: default_validators
					error_msg: default_error_msg
				}
				'st': {
					id: 'state_aid'
					title: 'State Aid'
					help: """Alabama state grant – other state aid only if you are
					from a reciprocal state (NOT FL,GA or LA)"""
					default: ''
					type: 'fin_aid'
					decorators: default_decorators.money
					validators: default_validators
					error_msg: default_error_msg
				}
				'out': {
					id: 'outside_res'
					title: 'Outside Resources'
					help: """Scholarship or grant from any organization outside of
					Spring Hill College but for use at Spring Hill College"""
					default: ''
					type: 'fin_aid'
					decorators: default_decorators.money
					validators: default_validators
					error_msg: default_error_msg
				}
				'sub': {
					id: 'sub_loans'
					title: 'Subsidized Loans'
					help: """$3500 max freshman year; $4500 max sophomore year;
					$5500 junior and senior year
					"""
					default: ''
					type: 'fin_aid'
					decorators: default_decorators.money
					validators: {
						number: true
						min: 0
						max: 5500
						step: '100'
					}
					error_msg: {
						number: default_error_msg.number
						min: 'This value must be in range 0 to 5500'
						max: 'This value must be in range 0 to 5500'
					}
				}
				'unsub': {
					id: 'unsub_loans'
					title: 'Unsubsidized Loans'
					default: ''
					help: """Additional $2000 per year on top of subsidized; or amounts
					listed for subsidized if you have no financial aid need"""
					type: 'fin_aid'
					decorators: default_decorators.money
					validators: {
						number: true
						min: 2000
						max: 9000
						step: '100'
					}
					error_msg: {
						number: default_error_msg.number
						min: 'This value must be in range 2000 to 9000'
						max: 'This value must be in range 2000 to 9000'
					}
				}
				'par': {
					id: 'parent_contrib'
					title: 'Student or Parent Resource Contributions'
					help: """Money saved, or accessible, to contribute toward college costs"""
					default: ''
					type: 'user_contrib'
					decorators: default_decorators.money
					validators: default_validators
					error_msg: default_error_msg
				}
				'plus': {
					id: 'parent_plus'
					title: 'Parent Plus Loans'
					default: ''
					help: """Students legal parent can borrow up to the cost of attendance to
					pay for their child's direct  educational costs"""
					type: 'user_contrib'
					decorators: default_decorators.money
					validators: default_validators
					error_msg: default_error_msg
				}
			}

		$scope.user_data.init = () ->
			for key,item of this.data
				item.amt = item.default

		$scope.user_data.get_by_type = (type) ->
			results = []
			for key,item of this.data
				if item.type is type
					results.push(item)
			return results

		$scope.user_data.load_data_from_qs = () ->
			qs = $location.search()
			for key,item of $scope.user_data.data
				if qs[key]
					# console.log key
					$scope.ui.has_qs_data = true
					item.amt = parseInt(qs[key])


		$scope.user_costs = (idx) ->
			cost_t = $scope.utils._yearly_cost_scale($scope.sel_student_type().tuition_first_yr, $scope.factors.tuition_perc_increase_yr, idx)
			cost_r = $scope.utils._yearly_cost_scale($scope.sel_student_type().room_board_first_yr, $scope.factors.room_board_perc_increase_yr, idx)
			fin_aid = $scope.utils.total_amounts($scope.user_data.get_by_type('fin_aid'))
			if $scope.user_data.data.unsub.amt > 0
				fin_aid = fin_aid + $scope.factors.fed_loan_yearly_increase[idx-1]
			user_contrib = $scope.utils.total_amounts($scope.user_data.get_by_type('user_contrib'))
			tot_aid = $scope.utils.total_amounts($scope.user_data.get_by_type('fin_aid')) + $scope.utils.total_amounts($scope.user_data.get_by_type('user_contrib'))
			return {
				'tuition': {
					'title': 'Tuition'
					'help': "Reflects average #{ $scope.factors.tuition_perc_increase_yr }% yearly tuition increase"
					'amt': cost_t.formatMoney(0)
					'decorators': ['$', '+']
				}
				'room_board': {
					'title': 'Room and Board'
					'help': "Reflects average #{ $scope.factors.room_board_perc_increase_yr }% yearly tuition increase"
					'amt': cost_r.formatMoney(0)
					'decorators': ['$', '+']
				}
				'total_costs': {
					'title': 'Total Costs'
					'amt': (cost_t + cost_r).formatMoney(0)
					'decorators': ['$', '=']
				}
				'total_aid': {
					'title': 'Total Eligible Financial Aid (from worksheet)'
					'amt': fin_aid.formatMoney(0)
					'decorators': ['$', '-']
				}
				'remaining': {
					'title': 'Remaining Balance'
					'amt': ((cost_t + cost_r) - fin_aid).formatMoney(0)
					'decorators': ['$', '=']
					'class': 'account'
				}
				'contributions': {
					'title': 'Total User Contributions (from worksheet)'
					'amt': user_contrib.formatMoney(0)
					'decorators': ['$', '-']
				}
				'balance': {
					'title': "Additional Funds Needed Year #{idx}"
					'amt': ((cost_t + cost_r) - tot_aid).formatMoney(0)
					'decorators': ['$', '=']
					'class': 'account big'
				}
			}

		$scope.ui = {
			has_dirty_data: false
			has_qs_data: false
			has_local_storage: -> return localStorageService.isSupported
			has_stored_data: -> return local_storage_key in localStorageService.keys()
			alerts: []
			# alerts = array of {"msg": msg, "type": type}
			addAlert: (msg, type = "info") ->
				# console.log "adding alert type " + type
				this.alerts.push({msg:msg, type:type})
			closeAlert: (index) ->
				this.alerts.splice index, 1
			findAlertIdxByMsg: (msg) ->
				if not this.alerts.length
					return -1
				for i in [0..this.alerts.length-1]
					if this.alerts[i].msg is msg
						return i
				return -1
			closeAlertByMsg: (msg) ->
				idx = this.findAlertIdxByMsg(msg)
				if idx isnt -1
					this.closeAlert(idx)
			loaded: false
			cleared: false
			strings: ui_strings
		}
		$scope.ui.saved = not $scope.ui.has_stored_data()
		$scope.ui.actions = {
			save: {
				help: "Save this affordability data in this browser. This will overwrite existing saved data."
				get_txt: -> if $scope.ui.saved then "Saved" else "Save data"
			}
			load: {
				help: "Load existing affordability data stored in this browser."
				get_txt: -> if $scope.ui.loaded then "Loaded" else "Load data"
			}
			clear: {
				help: "Clear all affordability data stored in this browser."
				get_txt: -> if $scope.ui.cleared then "Cleared" else "Clear data"
			}
		}
		$scope.ui.tour_stops = [
		      {
		        title: "Need help?"
		        content: 'Would you like to take a quick tour of this application? Click "Next" to proceed or "End tour" to cancel. You can always start the tour again later by clicking the "Show me what to do button".'
		        orphan: true
		      },
		      {
		        element: '#student_type_select'
		        title: "Who are you?"
		        content: "Tell us what kind of student you will be. We will pre-populate some of the fields with average cost information."
		      },
		      {
		        element: '#finaid_block'
		        title: "What kind of financial aid do you have?"
		        content: "The next 6 boxes represent your total <strong>official</strong> financial aid award. Fill out each field with the appropriate amount."
		        placement: 'top'
		      },
		      {
		        element: '#user_contrib_block'
		        title: "Do you or your parents have other money set aside for college?"
		        content: "The next 2 boxes are for student and parent contributions other than official financial aid awards. Fill out each field with the appropriate amount."
		        placement: 'top'
		      },
		      {
		        element: '#section_year_1'
		        title: "Here's your cost breakdown"
		        content: "Each column breaks down the cost of going to college each year, accounting for expected increases in tuition and financial aid. By the way, these boxes are not editable."
		        placement: 'top'
		      },
		      {
		        element: '#balance_1'
		        title: "Here's your out of pocket cost for the year"
		        content: "This is the amount you will need to come up with from other sources for each year."
		        placement: 'right'
		      },
		      {
		        element: '#shc_schol_grants_help'
		        title: "More helpful hints"
		        content: "Click the question mark icons for more details on specific items."
		        placement: 'top'
		        },
		      {
		        element: '#disclaimer'
		        title: "Very important!"
		        content: "Please read this important information about the accuracy of the Affordability Estimator"
		        placement: 'top'
		      },
		      {
		        element: '#privacy'
		        title: "Even more important!"
		        content: "This application does not gather ANY information about you! Read the details here."
		        placement: 'top'
		      },
		      {
		        element: '#start_tour'
		        title: "Thanks!"
		        content: 'You can replay this tour at any time by clicking this button.'
		      },
		    ]

		$scope.utils = {
			_yearly_cost_scale: (first_year_cost, factor, idx) ->
				return (first_year_cost * Math.pow(1 + factor/100, idx - 1))

			total_amounts: (amounts) ->
				t = 0
				for k,v of amounts
					amt = parseInt(v['amt'])
					if amt
						t = t + amt
				return t

			make_qs: () ->
				qs = []
				for key, item of $scope.user_data.data
					qs.push([key, item.amt].join('='))
				return qs.join('&')

			permalink: () ->
				return [$location.absUrl().split(/[#\?]/, 1), this.make_qs()].join("#?")

			save_local_data: () ->
				data = angular.toJson($scope.user_data.data)
				$scope.ui.saved = localStorageService.set(local_storage_key, data)
				$scope.ui.cleared = false
				$scope.ui.has_dirty_data = false
				$scope.ui.closeAlertByMsg ui_strings.alerts.form_preloaded

			load_local_data: () ->
				$scope.user_data.data = angular.fromJson(localStorageService.get(local_storage_key))
				$scope.ui.loaded = true
				$scope.ui.cleared = false
				$scope.ui.has_dirty_data = false
				$scope.ui.closeAlertByMsg ui_strings.alerts.has_stored_data
				$scope.ui.closeAlertByMsg ui_strings.alerts.form_preloaded

			clear_local_data: () ->
				$scope.ui.cleared = localStorageService.clearAll()
				$scope.ui.saved = false
				$scope.ui.loaded = false
				$scope.ui.has_dirty_data = false
		}


		# initialize inputs
		$scope.user_data.init()
		# if we have qs data, load it and post message
		$scope.user_data.load_data_from_qs()

		if $scope.ui.has_qs_data
			$scope.ui.addAlert ui_strings.alerts.form_preloaded, 'success'

		if $scope.ui.has_stored_data()
			$scope.ui.addAlert ui_strings.alerts.has_stored_data, 'success'

		make_dirty = (n,o) ->
			$scope.ui.has_dirty_data = true
			$scope.ui.saved = false
			$scope.ui.loaded = false

		$scope.$watch('user_data.data',
			(newValue, oldValue) -> make_dirty(newValue, oldValue),
			true)

	.config (localStorageServiceProvider) ->
		localStorageServiceProvider
			.setPrefix('fourYearAffordability')
