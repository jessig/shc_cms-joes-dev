from django.conf.urls import patterns, include, url
from .views import PrivateDirectoryList
from django.views.decorators.vary import vary_on_headers
urlpatterns = patterns('apps.views',
                       url(r'^dir/private/(?P<slug>[\w]+)/$',
                           vary_on_headers('Authorization')(
                               PrivateDirectoryList.as_view()),
                           name='print_dir'),
                       )
