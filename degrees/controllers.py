from __future__ import unicode_literals
from degrees.models import AcademicArea, Degree


def get_degrees(get_acad_level='ug'):
    areas = AcademicArea.objects.filter(level=get_acad_level)
    degrees = Degree.objects.filter(academic_area__in=areas)
    return degrees
