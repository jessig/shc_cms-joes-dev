from django.contrib import admin
from .models import AcademicArea, MMC, Degree

from adminsortable2.admin import SortableAdminMixin

class DegreeAdmin(admin.ModelAdmin):
    list_display = ('title', 'academic_area')
    list_filter = ('academic_area__level',)
    ordering = ("-academic_area__level", "academic_area", "page_ref__title")

class AcademicAreaAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('title', 'level')
    list_filter = ('level',)
    ordering = ('ordering', '-level', 'title')

# Register your models here.
admin.site.register(AcademicArea, AcademicAreaAdmin)
admin.site.register(MMC)
admin.site.register(Degree, DegreeAdmin)
