# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0002_gallerycontent'),
    ]

    operations = [
        migrations.CreateModel(
            name='AcademicArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.CharField(max_length=10, verbose_name=b'Academic Level', choices=[(b'ug', b'Undergraduate'), (b'grad', b'Graduate'), (b'cs', b'Continuing Studies')])),
                ('title', models.CharField(max_length=100, verbose_name=b'Title')),
                ('intro', models.TextField(max_length=1000, verbose_name=b'Intro Blurb', blank=True)),
                ('image_slug', models.CharField(help_text=b'image must already exist in AWS S3 bucket', max_length=255, blank=True)),
            ],
            options={
                'ordering': ['title'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Degree',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dept_code', models.CharField(max_length=5, verbose_name=b'Department DB code', blank=True)),
                ('academic_area', models.ForeignKey(to='degrees.AcademicArea')),
            ],
            options={
                'ordering': ['page_ref__title'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MMC',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=2)),
                ('description', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='degree',
            name='mmc',
            field=models.ManyToManyField(to='degrees.MMC', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='degree',
            name='page_ref',
            field=models.ForeignKey(to='page.Page'),
            preserve_default=True,
        ),
    ]
