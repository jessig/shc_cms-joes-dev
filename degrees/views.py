from django.http import HttpResponse
from controllers import get_degrees
from django.template.loader import render_to_string
from django.shortcuts import redirect
from degrees.models import AcademicArea, MMC


def degrees_ajax_filter(request):
    if request.method == 'POST':
        title = request.POST.get('title').title() if request.POST.get('title').title() else None
        # area = int(request.POST.get('area')) or None
        # mmc = int(request.POST.get('mmc')) or None
        degreeses = get_degrees()
        if title:
            degreeses = degreeses.filter(page_ref__title__istartswith=title)
        # if area:
        #     selected_area = AcademicArea.objects.get(id=area)
        #     degreeses = degreeses.filter(academic_area=selected_area)
        # if mmc:
        #     mmc_object = MMC.objects.get(id=mmc)
        #     degreeses = degreeses.filter(mmc=mmc_object)
        html = render_to_string('include/degrees_result.html', {'degrees_list': degreeses})
        return HttpResponse(html)
    else:
        return redirect('/')
