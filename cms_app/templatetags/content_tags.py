from django import template
from django.conf import settings
from django.core.cache import cache
from django.db.models import F
from django.core.files.images import get_image_dimensions

from feincms.module.page.models import Page
# from feincms.content.medialibrary.models import MediaFileContent
from forms_builder.forms.models import Form
from degrees.models import AcademicArea, MMC, Degree
from jobs.models import JobPosting
from profiles.models import Profile
from home_features.models import HomeFeature
from page_partials.models import PagePartial
import collections

from random import sample
import textile
from bs4 import BeautifulSoup

register = template.Library()


@register.simple_tag()
def get_image_orientation(mediafile):
    width, height = get_image_dimensions(mediafile.file)
    if width > height:
        return 'landscape'
    return 'portrait'


@register.assignment_tag()
def sub_entries(entry):
    return Page.objects.filter(parent_id=entry.id, in_navigation=True)


@register.assignment_tag()
def get_mmc():
    return MMC.objects.all()


@register.simple_tag()
def get_blog_or_page_title(entry, page):
    title = ""
    if entry:
        title = entry.title
    else:
        title = page.title
    return title


@register.simple_tag()
def get_blog_or_page_blurb(entry, page):
    text_block_classes = ["TextilePageContent",
        "RichTextContent",
        ]

    # return the page excerpt if we have one
    if page.excerpt_text:
        return page.excerpt_text

    item = page
    if entry:
        item = entry

    first_text_block = None
    for block in item.content.main:
        if block.__class__.__name__ in text_block_classes:
            first_text_block = block
            break
    
    if not first_text_block:
        return ""

    soup = BeautifulSoup(first_text_block.render(), 'html.parser')
    first_p = soup.p
    if soup.p:
        return soup.p.get_text()
    return ""


@register.assignment_tag()
def get_areas():
    return AcademicArea.objects.all()


@register.assignment_tag()
def get_degrees(get_acad_level):
    areas = AcademicArea.objects.filter(level=get_acad_level)
    return Degree.objects.filter(academic_area__in=areas)


@register.assignment_tag()
def get_home_features(section, num=6):
    features = HomeFeature.objects.filter(
        section=section, active=True).order_by('order')[:num]
    return features

@register.assignment_tag()
def get_page_by_id(id):
    try:
        page = Page.objects.get(pk=id)
    except:
        return None
    return page

@register.assignment_tag()
def get_partial_by_id(id):
    partial = PagePartial.objects.get(pk=id)
    return partial.get_rendered_content()


@register.assignment_tag()
def get_form_by_id(id):
    try:
        form = Form.objects.get(pk=id)
    except:
        form = None
    return form


@register.assignment_tag()
def get_active_job_postings_by_type(posting_type):
    return JobPosting.objects.filter(type=posting_type)


@register.assignment_tag()
def get_random_testimonials_for_page(page, num=3):
    attributions = []
    items = []
    if page.testimonials.exists():
        for tm in page.testimonials.all().order_by('?'):
            if tm.attribution not in attributions:
                items.append(tm)
                attributions.append(tm.attribution)
        if len(items) > num:
            return sample(items, num)
        return items
    return None


@register.assignment_tag()
def get_acad_area_img_path(acad_area_slug):
    return settings.SHC_CMS["image_paths"]["academic_area_header_image_pattern"] % acad_area_slug


@register.assignment_tag()
def get_grouped_degrees_by_level(get_acad_level):
    grouped_degrees = []
    areas = AcademicArea.objects.filter(level=get_acad_level)
    for area in areas:
        degrees = Degree.objects.filter(academic_area=area)
        data = {"area": area, "degrees": degrees}
        grouped_degrees.append(data)
    return grouped_degrees


@register.assignment_tag()
def get_degree_by_page_slug(slug):
    try:
        return Degree.objects.get(page_ref__slug=slug)
    except:
        return None


@register.assignment_tag()
def get_degrees_in_area(page, acad_area="self", exclude_page=False):

    majors_cache_secs = 60 * 60  # 1 hour

    page_degree = Degree.objects.get(page_ref__slug=page.slug)
    degrees = Degree.objects.all()

    # For some reason, this needs to come BEFORE filtering
    # for academic area
    if exclude_page:
        degrees = Degree.objects.exclude(id=page_degree.id)

    if acad_area == "self":
        degrees = degrees.filter(academic_area=page_degree.academic_area)
    elif acad_area:
        degrees = degrees.filter(academic_area=acad_area)

    data = {"area": page_degree.academic_area, "degrees": degrees}

    return data


@register.assignment_tag()
def get_dept_slug_by_degree(page):
    page_degree = Degree.objects.get(page_ref__slug=page.slug)
    return page_degree.dept_code


@register.assignment_tag()
def get_content_images(page, number=3):
    from feincms.content.medialibrary.models import MediaFileContent
    from gallery.models import GalleryContent

    og_images = []
    content_images = []
    gallery_images = []

    galleries = page.content.all_of_type(GalleryContent)
    if galleries:
        for image in galleries[0].gallery.ordered_images():
            try:
                gallery_images.append(image.get_absolute_url())
            except IOError:
                continue

    mediafiles = page.content.all_of_type(MediaFileContent)
    if mediafiles:
        for mediafilecontent in mediafiles:
            if mediafilecontent.mediafile.type == "image":
                try:
                    content_images.append(mediafilecontent.mediafile.file.url)
                except IOError:
                    continue

    og_images = gallery_images[:number] + content_images[:number]
    if settings.OG_IMAGE_DEFAULT:
        og_images.append(settings.OG_IMAGE_DEFAULT)

    return og_images


@register.assignment_tag()
def check_category_of_entry(entry, category_slug):
    return category_slug in [cat.get_translation().slug for cat in entry.categories.all()]


@register.assignment_tag()
def get_profiles_for_frontpage(featured, limit=2):
    return Profile.objects.filter(is_featured=featured)[:limit]
