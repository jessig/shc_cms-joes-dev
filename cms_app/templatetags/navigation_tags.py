from __future__ import absolute_import, unicode_literals
from feincms.module.page.models import Page
from django import template


register = template.Library()

# mptt is_leaf_node() does not know about active and in_navigation properties


def page_is_leaf(page):
    try:
        is_fake_leaf = page.fake_leaf
    except AttributeError:
        is_fake_leaf = False
    return is_fake_leaf or page.children.filter(active=True, in_navigation=True).count() == 0


@register.assignment_tag()
def section_links(feincms_page):
    try:
        page_is_leaf(feincms_page)
    except AttributeError:
        feincms_page = Page.objects.get(pk=feincms_page)
    if page_is_leaf(feincms_page):
        root = feincms_page.parent
        entries = feincms_page.get_siblings(include_self=True)
    else:
        root = feincms_page
        entries = feincms_page.get_children()
    entries = entries.filter(
        active=True, in_navigation=True, navigation_group='default')
    for entry in entries:
        entry.is_external = False
        # print entry.redirect_to,
        if entry.redirect_to and entry.redirect_to.startswith("http"):
            entry.is_external = True
        # print entry.is_external
    return {"root": root, "entries": entries, "page": feincms_page}


@register.assignment_tag()
def is_root(page):
    return page.override_url == "/"


# @register.assignment_tag()
# def sibling_links(feincms_page):
#     # We don't need anything on homepage
#     if feincms_page.level == 0:
#         entries = None
#     # We include level 1 items as a subnav, so don't need them here
#     elif feincms_page.level == 1:
#         entries = feincms_page.get_children()
#     else:
#         entries = feincms_page.get_siblings(include_self=True)
#     return entries

@register.assignment_tag()
def show_submenu_on_page(feincms_page):
    root = ancestor_page(feincms_page, 1).get_absolute_url().split('/')[1]
    if root in ['ug', 'grad']:
        return True
    return False


@register.assignment_tag()
def ancestor_page(feincms_page, level):
    page = feincms_page
    if page.level == level:
        return page
    while page.level > level:
        page = page.parent
    return page


@register.filter
def group_by_tree(iterable):
    parent = None
    children = []
    level = -1

    for element in iterable:
        if parent is None or element.level == level:
            if parent:
                yield parent, children
                parent = None
                children = []

            parent = element
            level = element.level
        else:
            children.append(element)

    if parent:
        yield parent, children
