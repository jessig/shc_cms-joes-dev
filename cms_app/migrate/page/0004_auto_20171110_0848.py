# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0003_auto_20170927_1454'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mediacategorylistingcontent',
            name='number',
            field=models.PositiveIntegerField(default=0, help_text='Leave blank to show all files.', verbose_name='number'),
            preserve_default=True,
        ),
    ]
