# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0002_gallerycontent'),
    ]

    operations = [
        migrations.AddField(
            model_name='mediacategorylistingcontent',
            name='number',
            field=models.PositiveIntegerField(default=0, help_text='Leave blank to show all files.', null=True, verbose_name='number', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='mediafilecontent',
            name='type',
            field=models.CharField(default='default', max_length=20, verbose_name='type', choices=[('default', 'default'), ('banner', 'banner'), ('captioned banner', 'banner w/ caption'), ('inline', 'inline'), ('captioned inline', 'inline w/ caption'), ('responsive', 'responsive'), ('captioned responsive', 'responsive w/ caption'), ('headshot', 'headshot'), ('captioned headshot', 'headshot w/ caption')]),
            preserve_default=True,
        ),
    ]
