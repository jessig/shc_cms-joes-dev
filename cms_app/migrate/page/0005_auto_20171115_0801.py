# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0004_auto_20171110_0848'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mediacategorylistingcontent',
            name='number',
            field=models.PositiveIntegerField(default=0, help_text='Set to zero to show all items.', verbose_name='number'),
            preserve_default=True,
        ),
    ]
