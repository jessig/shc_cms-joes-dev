# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0001_initial'),
        ('page', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='GalleryContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('gallery', models.ForeignKey(related_name='page_gallerycontent_gallery', to='gallery.Gallery', help_text='Choose a gallery to render here')),
                ('parent', models.ForeignKey(related_name='gallerycontent_set', to='page.Page')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'Image Galleries',
                'db_table': 'page_page_gallerycontent',
                'verbose_name': 'Image Gallery',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
    ]
