# uncompyle6 version 2.9.5
# Python bytecode 2.7 (62211)
# Decompiled from: Python 2.7.12 (default, Jul  1 2016, 15:12:24)
# [GCC 5.4.0 20160609]
# Embedded file name: /home/chughes/webapps/django/shc_cms/cms_app/migrate/medialibrary/0001_initial.py
# Compiled at: 2016-08-29 15:12:48
from __future__ import unicode_literals
from django.db import models, migrations
import django.utils.timezone
import feincms.extensions
import feincms.translations


class Migration(migrations.Migration):
    dependencies = []
    operations = [migrations.CreateModel(name=u'Category', fields=[(u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)), (u'title', models.CharField(max_length=200, verbose_name=u'title')), (u'slug', models.SlugField(max_length=150, verbose_name=u'slug')), (u'parent', models.ForeignKey(related_name=u'children', verbose_name=u'parent', blank=True, to=u'medialibrary.Category', null=True))], options={u'ordering': [u'parent__title', u'title'], u'verbose_name': u'category', u'verbose_name_plural': u'categories'}, bases=(models.Model,)), migrations.CreateModel(name=u'MediaFile', fields=[(u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)), (u'file', models.FileField(upload_to=u'medialibrary/%Y/%m/', max_length=255, verbose_name=u'file')), (u'type', models.CharField(verbose_name=u'file type', max_length=12, editable=False, choices=[(u'image', u'Image'), (u'video', u'Video'), (u'audio', u'Audio'), (u'pdf', u'PDF document'), (u'swf', u'Flash'), (u'txt', u'Text'), (u'rtf', u'Rich Text'), (u'zip', u'Zip archive'), (u'doc', u'Microsoft Word'), (u'xls', u'Microsoft Excel'), (u'ppt', u'Microsoft PowerPoint'), (u'other', u'Binary')])), (u'created', models.DateTimeField(
        default=django.utils.timezone.now, verbose_name=u'created', editable=False)), (u'copyright', models.CharField(max_length=200, verbose_name=u'copyright', blank=True)), (u'file_size', models.IntegerField(verbose_name=u'file size', null=True, editable=False, blank=True)), (u'categories', models.ManyToManyField(to=u'medialibrary.Category', verbose_name=u'categories', blank=True))], options={}, bases=(models.Model, feincms.extensions.ExtensionsMixin, feincms.translations.TranslatedObjectMixin)), migrations.CreateModel(name=u'MediaFileTranslation', fields=[(u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)), (u'language_code', models.CharField(default=u'en', verbose_name=u'language', max_length=10, editable=False, choices=[(u'en', u'English')])), (u'caption', models.CharField(max_length=200, verbose_name=u'caption')), (u'description', models.TextField(verbose_name=u'description', blank=True)), (u'parent', models.ForeignKey(related_name=u'translations', to=u'medialibrary.MediaFile'))], options={u'verbose_name': u'media file translation', u'verbose_name_plural': u'media file translations'}, bases=(models.Model,)), migrations.AlterUniqueTogether(name=u'mediafiletranslation', unique_together=set([(u'parent', u'language_code')]))]
# okay decompiling 0001_initial.pyc
