# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import feincms.module.mixins
import feincms.contrib.richtext
import feincms.module.medialibrary.fields
import feincms.module.extensions.datepublisher
import feincms.extensions
import feincms.translations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('medialibrary', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ordering', models.SmallIntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ['ordering'],
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
            },
            bases=(models.Model, feincms.translations.TranslatedObjectMixin),
        ),
        migrations.CreateModel(
            name='CategoryTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language_code', models.CharField(default='en', verbose_name='language', max_length=10, editable=False, choices=[('en', 'English')])),
                ('title', models.CharField(max_length=100, verbose_name='category title')),
                ('slug', models.SlugField(unique=True, verbose_name='slug')),
                ('description', models.CharField(max_length=250, verbose_name='description', blank=True)),
                ('parent', models.ForeignKey(related_name='translations', to='elephantblog.Category')),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'category translation',
                'verbose_name_plural': 'category translations',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True, db_index=True, verbose_name='is active')),
                ('is_featured', models.BooleanField(default=False, db_index=True, verbose_name='is featured')),
                ('title', models.CharField(max_length=100, verbose_name='title')),
                ('slug', models.SlugField(max_length=100, verbose_name='slug', unique_for_date='published_on')),
                ('published_on', models.DateTimeField(default=django.utils.timezone.now, blank=True, help_text='Will be filled in automatically when entry gets published.', null=True, verbose_name='published on', db_index=True)),
                ('last_changed', models.DateTimeField(auto_now=True, verbose_name='last change')),
                ('publication_date', models.DateTimeField(default=feincms.module.extensions.datepublisher.granular_now, verbose_name='publication date')),
                ('publication_end_date', models.DateTimeField(help_text='Leave empty if the entry should stay active forever.', null=True, verbose_name='publication end date', blank=True)),
                ('creation_date', models.DateTimeField(verbose_name='creation date', null=True, editable=False)),
                ('modification_date', models.DateTimeField(verbose_name='modification date', null=True, editable=False)),
                ('author', models.ForeignKey(related_name='blogentries', verbose_name='author', to=settings.AUTH_USER_MODEL)),
                ('categories', models.ManyToManyField(related_name='blogentries', null=True, verbose_name='categories', to='elephantblog.Category', blank=True)),
            ],
            options={
                'ordering': ['-published_on'],
                'get_latest_by': 'published_on',
                'verbose_name': 'entry',
                'verbose_name_plural': 'entries',
            },
            bases=(models.Model, feincms.extensions.ExtensionsMixin, feincms.module.mixins.ContentModelMixin),
        ),
        migrations.CreateModel(
            name='MediaFileContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('type', models.CharField(default='default', max_length=20, verbose_name='type', choices=[('default', 'default'), ('banner', 'banner'), ('captioned banner', 'banner w/ caption'), ('inline', 'inline'), ('captioned inline', 'inline w/ caption'), ('responsive', 'responsive'), ('captioned responsive', 'responsive w/ caption')])),
                ('mediafile', feincms.module.medialibrary.fields.MediaFileForeignKey(related_name='+', verbose_name='media file', to='medialibrary.MediaFile')),
                ('parent', models.ForeignKey(related_name='mediafilecontent_set', to='elephantblog.Entry')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'media files',
                'db_table': 'elephantblog_entry_mediafilecontent',
                'verbose_name': 'media file',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RawContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(verbose_name='content', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='rawcontent_set', to='elephantblog.Entry')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'raw contents',
                'db_table': 'elephantblog_entry_rawcontent',
                'verbose_name': 'raw content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RichTextContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', feincms.contrib.richtext.RichTextField(verbose_name='text', blank=True)),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='richtextcontent_set', to='elephantblog.Entry')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'rich texts',
                'db_table': 'elephantblog_entry_richtextcontent',
                'verbose_name': 'rich text',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TextilePageContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('show_on', models.DateTimeField(help_text='leave blank to publish now', null=True, blank=True)),
                ('hide_after', models.DateTimeField(help_text='leave blank for no expiration', null=True, blank=True)),
                ('content', models.TextField()),
                ('region', models.CharField(max_length=255)),
                ('ordering', models.IntegerField(default=0, verbose_name='ordering')),
                ('parent', models.ForeignKey(related_name='textilepagecontent_set', to='elephantblog.Entry')),
            ],
            options={
                'ordering': ['ordering'],
                'abstract': False,
                'verbose_name_plural': 'textile page contents',
                'db_table': 'elephantblog_entry_textilepagecontent',
                'verbose_name': 'textile page content',
                'permissions': [],
            },
            bases=(models.Model,),
        ),
    ]
