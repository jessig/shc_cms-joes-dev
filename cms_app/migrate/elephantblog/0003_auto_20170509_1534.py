# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0001_initial'),
        ('elephantblog', '0002_gallerycontent'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallerycontent',
            name='gallery',
            field=models.ForeignKey(related_name='elephantblog_gallerycontent_gallery', to='gallery.Gallery', help_text='Choose a gallery to render here'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='gallerycontent',
            name='parent',
            field=models.ForeignKey(related_name='gallerycontent_set', to='elephantblog.Entry'),
            preserve_default=True,
        ),
    ]
