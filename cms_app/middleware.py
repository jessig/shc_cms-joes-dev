from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.core.exceptions import MiddlewareNotUsed
from django.http import HttpResponsePermanentRedirect, HttpResponseRedirect
from django.contrib.redirects.middleware import RedirectFallbackMiddleware


class ForceDomainMiddleware(object):

    def __init__(self):
        if settings.DEBUG:
            raise MiddlewareNotUsed

    def process_request(self, request):
        if request.method != 'GET':
            return

        domain = getattr(settings, 'FORCE_DOMAIN', None)

        if not domain:
            return

        if request.META['HTTP_HOST'] != domain:
            target = 'http%s://%s%s' % (
                request.is_secure() and 's' or '',
                domain,
                request.get_full_path())
            return HttpResponsePermanentRedirect(target)


class SHCRedirectFallbackMiddleware(RedirectFallbackMiddleware):
    response_redirect_class = HttpResponseRedirect
