from __future__ import absolute_import, unicode_literals

from django.db import models

from django.utils.translation import ugettext_lazy as _

from feincms.module.page.models import Page
from feincms.module.page.modeladmins import PageAdmin
from feincms.content.application.models import ApplicationContent
from feincms.content.raw.models import RawContent
from feincms.content.richtext.models import RichTextContent
from feincms.content.template.models import TemplateContent
from feincms.content.medialibrary.models import MediaFileContent
from profiles.models import ProfileContent

from feincms.extensions import Extension
from feincms.module.medialibrary.fields import MediaFileForeignKey
from feincms.module.medialibrary.models import MediaFile
from feincms.module.page.extensions.navigationgroups import Extension as NavGroupExtension

from elephantblog.models import Entry

from feincms import settings

from feincms_cleanse import Cleanse
# from feincms_cleanse import cleanse_html

from feincms_links.models import LinkContent

from forms_builder_integration.models import FormContent

from cms_app.cms.contents import (SubpageContent,
                                  SlideContent,
                                  PageTeaserContent,
                                  TextilePageContent,
                                  PagePartialContent,
                                  MediaCategoryListingContent,
                                  DirectoryListingContent,
                                  CampusMapContent,
                                  CounselorMapContent,
                                  AfforabilityCalcContent,
                                  NewsListingContent,
                                  ModalFormContent,
                                  JumpLinksContent)
from testimonials.models import Testimonial
from profiles.models import Profile
from gallery.models import GalleryContent


class Cleanser(Cleanse):
    allowed_tags = {
        'a': ('href', 'name', 'target', 'title'),
        'h2': (),
        'h3': (),
        'strong': (),
        'em': (),
        'p': ('class'),
        'div': ('class'),
        'ul': (),
        'ol': (),
        'li': (),
        'span': (),
        'br': (),
        'sub': (),
        'sup': (),
        'table': (),
        'tbody': (),
        'thead': (),
        'th': ('colspan'),
        'tr': (),
        'td': ('colspan'),
    }


def cleanse_html(html):
    """
    Compat shim for older cleanse API
    """
    # print "using Cleanser"
    return Cleanser().cleanse(html)

MEDIA_TYPE_CHOICES = (
    ('default', _('default')),
    ('banner', _('banner')),
    ('captioned banner', _('banner w/ caption')),
    ('inline', _('inline')),
    ('captioned inline', _('inline w/ caption')),
    ('responsive', _('responsive')),
    ('captioned responsive', _('responsive w/ caption')),
    ('headshot', _('headshot')),
    ('captioned headshot', _('headshot w/ caption')),
)

Profile.register_extensions(
    'feincms.module.page.extensions.titles',
)
Profile.register_regions(
    ('main', _('Main content area')),
)
Profile.create_content_type(RichTextContent,
                            #   cleanse=cleanse_html,
                            regions=('main',),
                            class_name="RichTextProfileContent"
                            )
Profile.create_content_type(
    TextilePageContent,
    regions=('main',),
    class_name="TextileProfileContent"
)
Profile.create_content_type(
    MediaFileContent,
    TYPE_CHOICES=MEDIA_TYPE_CHOICES,
    regions=('main',),
    class_name="ProfileMediaFileContent"
)
Profile.create_content_type(
    RawContent,
    regions=('main',),
    class_name="ProfileRawContent"
)


# class AliasExtension(Extension):
#
#    def handle_model(self):
#        self.model.add_to_class(
#            'alias',
#            models.CharField(
#                _('URL vanity alias'),
#                blank=True,
#                null=True,
#                max_length=100,
#                default='',
#                help_text="This should be an absolute path, excluding the domain name. Example: '/events/search/'."
#            ),
#        )
#
#    def handle_modeladmin(self, modeladmin):
#        modeladmin.add_extension_options(_('Vanity URL'), {
#            'fields': ('alias',),
#            'classes': ('collapse',),
#        })


Entry.register_extensions(
    'feincms.module.extensions.datepublisher',
    'feincms.module.extensions.changedate',
    # AliasExtension
)

Entry.register_regions(
    ('main', _('Main content area')),
)
Entry.create_content_type(RichTextContent,
                          #   cleanse=cleanse_html,
                          regions=('main',),
                          )
Entry.create_content_type(
    TextilePageContent,
    regions=('main',)
)
Entry.create_content_type(
    MediaFileContent,
    TYPE_CHOICES=MEDIA_TYPE_CHOICES,
    regions=('main',)
)
Entry.create_content_type(
    GalleryContent,
    regions=('main',)
)
Entry.create_content_type(
    RawContent,
    regions=('main',),
)

Page.register_templates(
    {
        'key': 'bricks_left',
        'title': _('Bricks with left sidebar'),
        'path': 'grid/bricks_two-col_left-sidebar.html',
        'regions': (
            ('main', _('Main Content Area')),
            ('banner', _('Top Banner')),
            ('sidebar', _('Sidebar')),
            ('foot', _('Content Foot')),
        ),
    },
    {
        'key': 'single_col_stacked',
        'title': _('Single Column'),
        'path': 'grid/single_col_stacked.html',
        'regions': (
            ('main', _('Main Content Area')),
        ),
    },
    {
        'key': 'finaid_page',
        'title': _('Finaid Page'),
        'path': 'grid/ug_finaid_page.html',
        'regions': (
            ('main', _('Main Content Area')),
            ('row1', _('Row 1 Subcontent')),
            ('row2', _('Row 2 Subcontent')),
            ('banner', _('Top Banner')),
            ('sidebar', _('Sidebar')),
            ('foot', _('Content Foot')),
        ),
    },
    {
        'key': 'sellsheet',
        'title': _('For UG Academic Sellsheets'),
        'path': 'content/sellsheet.html',
        'regions': (
            ('main', _('Main Content Area')),
            ('row1', _('Row 1 Subcontent')),
            ('row2', _('Row 2 Subcontent')),
            ('banner', _('Top Banner')),
            ('sidebar', _('Sidebar')),
            ('foot', _('Content Foot')),
        ),
    },
    {
        'key': 'landing',
        'title': _('Landing Pages'),
        'path': 'grid/landing_page.html',
        'regions': (
            ('banner', _('Top Banner')),
            ('main', _('Main Content Area')),
            ('sidebar', _('Sidebar')),
            ('foot', _('Content Foot')),
        ),
    },
    {
        'key': 'home',
        'title': _('Site homepage'),
        'path': 'index.html',
        'regions': (
            ('main', _('Main Content Area')),
            ('banner', _('Top Banner')),
            ('foot', _('Content Foot')),
        ),
    },
)


class ExcerptExtension(Extension):

    def handle_model(self):
        self.model.add_to_class(
            'excerpt_image',
            MediaFileForeignKey(
                MediaFile, verbose_name=_('image'),
                blank=True, null=True, related_name='+'))
        self.model.add_to_class(
            'excerpt_text',
            models.TextField(_('text'), blank=True))

    def handle_modeladmin(self, modeladmin):
        modeladmin.raw_id_fields.append('excerpt_image')
        modeladmin.add_extension_options(_('Excerpt'), {
            'fields': ('excerpt_image', 'excerpt_text'),
        })

"""
Add a many-to-many relationship field to relate this page to other pages.
"""


class TestimonialsExtension(Extension):

    def handle_model(self):
        self.model.add_to_class('testimonials', models.ManyToManyField(
            Testimonial,
            blank=True,
            null=True,
            related_name='testimonials',
            help_text=_(
                'Select testimonials that should be related to this page.')))

    def handle_modeladmin(self, modeladmin):
        modeladmin.extend_list('filter_horizontal', ['testimonials'])

        modeladmin.add_extension_options(_('Testimonials'), {
            'fields': ('testimonials',),
            'classes': ('collapse',),
        })


class NavigationGroupExtension(NavGroupExtension):
    groups = [
        ('default', _('Default')),
        ('meta', _('Meta')),
        ('subnav1', _('Subnav 1')),
        ('subnav2', _('Subnav 2')),
        ('subnav3', _('Subnav 3')),
    ]


class FakeLeafExtension(Extension):

    def handle_model(self):
        self.model.add_to_class(
            'fake_leaf',
            models.BooleanField(
                _('fake leaf'),
                default=False,
                help_text="Treat this page as if it has no children"
            ),
        )

    def handle_modeladmin(self, modeladmin):
        modeladmin.add_extension_options('fake_leaf')
        # modeladmin.extend_list('list_display', ['fake_leaf'])
        modeladmin.extend_list('list_filter', ['fake_leaf'])


Page.register_extensions('feincms.module.extensions.ct_tracker',
                         'feincms.module.extensions.datepublisher',
                         'feincms.module.extensions.changedate',
                         # 'feincms.module.page.extensions.excerpt',
                         # the built in excerpt does not supply an image
                         # field, build our own
                         ExcerptExtension,
                         # 'feincms.module.page.extensions.sites',
                         'feincms.module.page.extensions.symlinks',
                         'feincms.module.extensions.featured',
                         'feincms.module.page.extensions.navigation',
                         'feincms.module.page.extensions.relatedpages',
                         'feincms.module.page.extensions.titles',
                         # 'feincms.module.page.extensions.navigationgroups',
                         NavigationGroupExtension,
                         FakeLeafExtension,
                         #  AliasExtension,
                         # PageVarsExtension,
                         TestimonialsExtension,
                         )

DEFAULT_REGIONS = ['banner', 'main', 'main2',
                   'row1', 'row2', 'sidebar', 'foot']

Page.create_content_type(
    TextilePageContent,
    optgroup='Content',
    regions=DEFAULT_REGIONS
)
Page.create_content_type(
    RichTextContent,
    cleanse=cleanse_html,
    optgroup='Content',
    regions=DEFAULT_REGIONS
)
Page.create_content_type(
    RawContent,
    optgroup='Content'
)
Page.create_content_type(
    PagePartialContent,
    optgroup='Content',
    regions=DEFAULT_REGIONS
)
Page.create_content_type(
    SlideContent,
    regions=('gallery',)
)
Page.create_content_type(
    MediaFileContent,
    TYPE_CHOICES=MEDIA_TYPE_CHOICES,
    optgroup='Dynamic',
    regions=DEFAULT_REGIONS
)
Page.create_content_type(
    GalleryContent,
    optgroup='Dynamic',
    regions=DEFAULT_REGIONS
)
Page.create_content_type(
    SubpageContent,
    optgroup='Content',
    regions=DEFAULT_REGIONS
)
Page.create_content_type(
    PageTeaserContent,
    optgroup='Content',
    regions=('sidebar')
)
Page.create_content_type(
    FormContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic'
)
Page.create_content_type(
    TemplateContent,
    optgroup='Dynamic',
    regions=DEFAULT_REGIONS
)
Page.create_content_type(
    DirectoryListingContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic',
)
Page.create_content_type(
    CampusMapContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic',
)
Page.create_content_type(
    CounselorMapContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic',
)
Page.create_content_type(
    AfforabilityCalcContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic',
)
Page.create_content_type(
    NewsListingContent,
    optgroup='Content',
    regions=DEFAULT_REGIONS
)
Page.create_content_type(
    ProfileContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic',
)
Page.create_content_type(
    ModalFormContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic',
)
Page.create_content_type(
    MediaCategoryListingContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic',
)
Page.create_content_type(
    LinkContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic',
)
Page.create_content_type(
    JumpLinksContent,
    regions=DEFAULT_REGIONS,
    optgroup='Dynamic'
)
Page.create_content_type(
    ApplicationContent,
    APPLICATIONS=(
        ('jobs.urls', 'Jobs application'),
        ('elephantblog.urls', 'Blog'),
        ('magazine.urls', 'Magazine'),
    )
)

# from .admin import save_page
# PageAdmin.save_model = save_page
#
# from elephantblog.modeladmins import EntryAdmin
# EntryAdmin.save_model = save_page
