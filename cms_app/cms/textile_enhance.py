from __future__ import absolute_import
from feincms.module.medialibrary.models import MediaFile
from feincms.module.page.models import Page
from feincms_links.models import Link
from page_partials.models import PagePartial

import textile

from django.conf import settings
# from django_thumbor import generate_url
from easy_thumbnails.files import get_thumbnailer
import re


def obj_content(match):
    # print "GOT OBJ CONTENT REF!!! %s:%s" % (match.group(1), match.group(2))
    if match:
        content_type = None
        url = ""
        if match.group(1) == "pagepartial":
            content_type = PagePartial
            content_field_name = "content"
        try:
            content = content_type.objects.get(pk=match.group(2))
            content = getattr(content, content_field_name)
        except:
            # We should let somebody know if there is not
            # a match for this id!!!!!!!!!!!
            content = ""
        return content


def obj_ref_url(match):
    # print "GOT OBJ REF!!! %s" % match.group(1)
    if match:
        content_type = None
        url = ""
        if match.group(1) == "page":
            content_type = Page
        if match.group(1) == "mediafile":
            content_type = MediaFile
        if match.group(1) == "link":
            content_type = Link
        try:
            url = content_type.objects.get(
                pk=match.group(2)).get_absolute_url()
        except:
            # We should let somebody know if there is not
            # a match for this id!!!!!!!!!!!
            url = match.group(0)
        return url


def thumbor_url(match):
    if match:
        # If we are in development mode, the thumbor server may not
        # have access to the site.
        # Fake thumbor images by setting simple image classes which are styled
        # by the theme
        if settings.DEBUG:
            return "!(image-%s)%s!" % (match.group("key"), match.group("url"))
        try:
            thumbor_settings = settings.SHC_CMS[
                "image_thumbs"][match.group("key")]
        # set defaults if we don't find a settings key
        except:
            thumbor_settings = {"width": 600, "height": 0, "crop": True}
        print match.group("url")
        if match.group("url"):
            resized = get_thumbnailer(
                match.group("url")).get_thumbnail(thumbor_settings).url
        classes = match.group("classes") or ""
        return "!%s%s!" % (classes, resized)


def wrap_email(match):
    if match:
        addy = match.group()
        postaddy = ""
        # remove trailing period in cases where email addy ended sentence
        if addy[-1] == ".":
            addy = addy[:-1]
            postaddy = "."
        return '<a href="mailto:%s">%s</a>%s' % (addy,  addy, postaddy)


def enhance_textile(content, region='main'):

    OBJ_CONTENT = re.compile(r"\[content\/(\w+)\/([0-9]+)\]")
    content = OBJ_CONTENT.sub(obj_content, content)

    OBJ_REFERENCE = re.compile(r"\[(\w+)\/([0-9]+)\]")
    content = OBJ_REFERENCE.sub(obj_ref_url, content)

    # thumbor thumbnail tags look like this:
    # !thumbor(settings_key) (class image-class)http://image.url/image.jpg!
    THUMBOR_URL_REGEX = re.compile(
        r"!thumbor(\((?P<key>\w+)\))?\s(?P<classes>\(\w+\))?(?P<url>[\w\:\?\/\.\-\=\&\%]+)!")
    content = THUMBOR_URL_REGEX.sub(thumbor_url, content)

    EMAIL_REGEX = re.compile(r"([\w.-]+)@([\w.-]+)")
    content = EMAIL_REGEX.sub(wrap_email, content)

    small_regions = ('sidebar',)
    head_offset = 1
    if region in small_regions:
        head_offset = 2

    return textile.textile(content, head_offset=head_offset, html_type='html5')
