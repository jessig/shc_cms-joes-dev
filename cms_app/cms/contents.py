from __future__ import absolute_import, unicode_literals

from django.db import models
from django.template.loader import render_to_string
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe

from django.conf import settings
from django import forms

from feincms.admin import item_editor
from feincms.admin.item_editor import FeinCMSInline
from feincms.content.medialibrary.models import MediaFileContent
from feincms.module.medialibrary.fields import (
    ContentWithMediaFile, MediaFileForeignKey)
from feincms.module.medialibrary.models import MediaFile, Category
from feincms.module.page.models import Page
from page_partials.models import PagePartial

from feincms_links.models import Link

from elephantblog.models import Entry

# from feincms_links.models import Category as LinkContent

from forms_builder.forms.models import Form

from django_thumbor import generate_url
import textile
from cms_app.cms.textile_enhance import enhance_textile

from datetime import datetime


class DateBasedPublishingMixin(models.Model):
    show_on = models.DateTimeField(
        blank=True,
        null=True,
        help_text='leave blank to publish now'
    )
    hide_after = models.DateTimeField(
        blank=True,
        null=True,
        help_text='leave blank for no expiration'
    )

    class Meta:
        abstract = True


class PagePartialContent(models.Model):
    partial = models.ForeignKey(PagePartial)

    class Meta:
        abstract = True

    def render(self, **kwargs):

        partial = PagePartial.objects.get(pk=self.partial.id)
        return enhance_textile(partial.content, self.region)


class SubpageContent(models.Model):
    style = models.CharField(
        _('style'),
        max_length=20,
        choices=(
            ('default', _('list')),
            ('simple', _('simple')),
            ('grid', _('grid')),
        ),
        default='default')
    start_page = models.ForeignKey(
        Page,
        blank=True,
        null=True,
        related_name="subpage_content",
        help_text='Leave blank to use current page'
    )

    class Meta:
        abstract = True
        verbose_name = _('subpages listing')
        verbose_name_plural = _('subpages listings')

    def render(self, **kwargs):
        if self.start_page:
            this = self.start_page
        else:
            this = self
            # use self if we are at the top level
            try:
                if this.parent:
                    this = this.parent
            except AttributeError:
                pass
        return render_to_string([
            'content/subpage/%s.html' % self.style,
            'content/subpage/default.html',
        ], {
            'content': this,
            'page_list': this.children.in_navigation().select_related(
                'excerpt_image'),
        })


class SlideContent(ContentWithMediaFile):
    title = models.CharField(_('title'), max_length=100, blank=True)
    subtitle = models.CharField(_('subtitle'), max_length=100, blank=True)
    area = models.CharField(
        _('preferred area if cropping'),
        max_length=10,
        choices=(
            ('50x50', _('center')),
            ('50x20', _('center / top')),
            ('20x50', _('left / center')),
            ('80x50', _('right / center')),
            ('50x80', _('center / bottom')),
        ),
        default='50x50')

    class Meta:
        abstract = True
        verbose_name = _('slide')
        verbose_name_plural = _('slides')

    def render(self, **kwargs):
        return render_to_string([
            'content/slide/default.html',
        ], {'content': self})

    @property
    def crop(self):
        return '950x360-%s' % self.area


class PageTeaserInline(FeinCMSInline):
    raw_id_fields = ('page', 'mediafile')


class PageTeaserContent(models.Model):
    feincms_item_editor_inline = PageTeaserInline

    page = models.ForeignKey(Page, verbose_name=_('page'), related_name='+')
    mediafile = MediaFileForeignKey(
        MediaFile, verbose_name=_('mediafile'), related_name='+')
    title = models.CharField(_('title'), max_length=100, blank=True)

    class Meta:
        abstract = True
        verbose_name = _('page teaser')
        verbose_name_plural = _('page teasers')

    @classmethod
    def get_queryset(cls, filter_args):
        return cls.objects.filter(filter_args).select_related(
            'parent', 'page', 'mediafile')

    def render(self, **kwargs):
        return render_to_string('content/pageteaser/default.html', {
            'content': self,
        })

    def save(self, *args, **kwargs):
        if not self.title:
            self.title = self.page.title
        if not self.mediafile:
            try:
                self.mediafile = self.page.content.all_of_type(
                    MediaFileContent)[0]
            except IndexError:
                pass
        super(PageTeaserContent, self).save(*args, **kwargs)


MEDIA_LIST_LAYOUT_CHOICES = (
    ('list-grid.html', 'Grid'),
    # ('image-gallery.html', 'Image Gallery')
)


class MediaCategoryListingContent(models.Model):
    category = models.ForeignKey(
        Category, verbose_name=_('category'), related_name='+')
    layout = models.CharField(
        max_length=25,
        choices=MEDIA_LIST_LAYOUT_CHOICES,
        default="list-grid.html"
    )
    number = models.PositiveIntegerField(
        _('number'), default=0, help_text=_('Set to zero to show all items.'))
    reverse_sort = models.BooleanField()

    class Meta:
        abstract = True
        verbose_name = _('media category listing')
        verbose_name_plural = _('media category listings')

    # @property
    # def media(self):
    #     return forms.Media(
    #         css={
    #             'all':
    #                 ('theme/vendor/justifiedGallery/dist/css/justifiedGallery.min.css',
    #                  'theme/vendor/lightgallery/dist/css/lightgallery.min.css',
    #                  'theme/styles/lightgallery.css',
    #                  )
    #         },
    #         js=('theme/vendor/justifiedGallery/dist/js/jquery.justifiedGallery.min.js',
    #             'theme/vendor/lightgallery/dist/js/lightgallery.min.js',
    #             'theme/vendor/lg-share/dist/lg-share.min.js',
    #             'theme/vendor/lg-pager/dist/lg-pager.min.js',
    #             'theme/scripts/image-gallery.min.js'),
    #     )

    def render(self, **kwargs):
        sorts = ['translations__caption', 'file']
        if self.reverse_sort:
            sorts = ["-" + sort for sort in sorts]
        items = MediaFile.objects.filter(
            categories=self.category).order_by(*sorts)
        if self.number:
            items = items[:self.number]
        return render_to_string('partials/media/%s' % self.layout, {
            'category': self.category,
            'items': items
        })




class DirectoryListingContent(models.Model):
    group_code = models.CharField(_('group code'), max_length=6,
                                  help_text='Use code "search" to crate a searchable directory not tied to a specific department.')
    query_options = models.CharField(_('query options'), max_length=100,
                                     blank=True, help_text="if you don't know what this is, don't use it")

    class Meta:
        abstract = True
        verbose_name = _('directory listing')
        verbose_name_plural = _('directory listings')

    @property
    def media(self):
        return forms.Media(
            css={'all': ('apps/styles/directory.css',), },
            js=("//ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js",
                "//ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular-resource.min.js",
                'apps/scripts/hb.js',
                "apps/scripts/facstaff-directory.js",
                ),
        )

    def render(self, **kwargs):
        if self.group_code == "search":
            return render_to_string('apps/directory/facstaff-directory-search.html')
        return render_to_string('apps/directory/group-contacts.html', {
            'group_code': self.group_code,
            'query_options': self.query_options
        })


class CampusMapContent(models.Model):

    class Meta:
        abstract = True
        verbose_name = _('map content')
        verbose_name_plural = _('map contents')

    @property
    def media(self):
        return forms.Media(
            css={'all': ('apps/styles/map.css',), },
            js=('//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js',
                '//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-route.min.js',
                '//maps.googleapis.com/maps/api/js?key=AIzaSyDDUwTu17HXTUyuYLSe9e22p36sjKhiHpk',
                'apps/scripts/map.js',),
        )

    def render(self, **kwargs):
        return render_to_string('apps/map/map.html')


class CounselorMapContent(models.Model):

    class Meta:
        abstract = True
        verbose_name = _('counselor map')
        verbose_name_plural = _('counselor maps')

    @property
    def media(self):
        return forms.Media(
            js=(
                '//maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDDUwTu17HXTUyuYLSe9e22p36sjKhiHpk',
                'apps/scripts/hb.js',
                'apps/scripts/counselor-map.js',),
        )

    def render(self, **kwargs):
        return render_to_string('apps/counselors/counselor-map.html')


class AfforabilityCalcContent(models.Model):

    class Meta:
        abstract = True
        verbose_name = _('affordability calculator')
        verbose_name_plural = _('affordability calculators')

    @property
    def media(self):
        return forms.Media(
            css={'all': ('apps/styles/afford.css',), },
            js=("//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js",
                "//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular-sanitize.min.js",
                "https://code.jquery.com/jquery-2.2.4.min.js",
                "theme/vendor/foundation/js/foundation.min.js",
                # "theme/vendor/foundation/js/foundation/foundation.joyride.js",
                "apps/vendor/angular-local-storage/dist/angular-local-storage.min.js",
                "apps/vendor/mm-foundation.min.js",
                "apps/scripts/afford.js",),
        )

    def render(self, **kwargs):
        return render_to_string('apps/afford/afford.html')


class NewsListingContent(models.Model):
    num_items = models.IntegerField(_('num items'))

    class Meta:
        abstract = True
        verbose_name = _('news listing')
        verbose_name_plural = _('news listings')

    def render(self, **kwargs):
        items = Entry.objects.all()[:self.num_items]
        return render_to_string('elephantblog/entry_archive_block.html',
                                {"object_list": items}
                                )


class ModalFormContent(models.Model):
    form_id = models.IntegerField(_('form id'), max_length=5)
    button_txt = models.CharField(_('button text'), max_length=100, blank=True)
    form_title = models.CharField(_('form title'), max_length=200, blank=True)

    class Meta:
        abstract = True
        verbose_name = _('Modal Form Embed')
        verbose_name_plural = _('Modal Form Embeds')

    def render(self, **kwargs):
        # we must use a RequestContext here so Django CSRF verification
        # works properly
        form = Form.objects.get(pk=self.form_id)
        request = kwargs['request']
        context = RequestContext(request,
                                 {
                                     'form': form,
                                     'button_txt': self.button_txt,
                                     'form_title': self.form_title
                                 })
        return render_to_string('partials/forms/modal-form-render.html', context)

# class LinkListingContent(models.Model):
#     category = models.ForeignKey(LinkCategory, verbose_name=_('category'), related_name='+')

#     class Meta:
#         abstract = True
#         verbose_name = _('link category listing')
#         verbose_name_plural = _('link category listings')

#     def render(self, **kwargs):
#         items = MediaFile.objects.filter(categories=self.category)
#         return render_to_string('partials/media/list-grid.html', {
#             'category': self.category,
#             'items': items
#         })


JL_LAYOUT_CHOICES = (
    ('block-links', 'block'),
    ('inline-links', 'inline'),
)

JL_STYLE_CHOICES = (
    ('bullets', 'with bullets'),
    ('no-bullets', 'without bullets'),
)


class JumpLinksContent(models.Model):
    layout = models.CharField(
        'Layout',
        max_length=15,
        choices=JL_LAYOUT_CHOICES,
        default='block-links',
    )
    style = models.CharField(
        'Link Style',
        max_length=15,
        choices=JL_STYLE_CHOICES,
        default='bullets',
    )
    category = models.SlugField(
        'Link Class Category',
        max_length=15,
        blank=True,
        help_text='Can contain only letters, numbers, and hyphens',
    )

    class Meta:
        abstract = True
        verbose_name = 'Jump Links Content'
        verbose_name_plural = 'Jump Links Content'

    def render(self, **kwargs):
        return render_to_string('partials/menus/jumplinks.html',
                                {"layout": self.layout, "style": self.style,
                                    "cat": self.category}
                                )


class TextileInline(item_editor.FeinCMSInline):
    fieldsets = (
        (None, {
            'fields': ('content', 'region', 'ordering')
        }),
        ('Advanced options', {
            'fields': ('show_on', 'hide_after'),
            'classes': ['collapse_inline'],
            # 'classes': ['collapse'],
        })
    )


class TextilePageContent(DateBasedPublishingMixin, models.Model):
    content = models.TextField()
    # rendered_content = models.TextField()

    feincms_item_editor_inline = TextileInline
    feincms_item_editor_includes = {
        'head': ['admin/content/init_inline_fieldsets.html'],
    }

    class Meta:
        abstract = True

    def render(self, **kwargs):

        now = datetime.now()

        if self.show_on and now < self.show_on:
            return ""

        if self.hide_after and now > self.hide_after:
            return ""

        small_regions = ('sidebar',)
        head_offset = 1
        if self.region in small_regions:
            head_offset = 2

        return mark_safe(enhance_textile(self.content, self.region))
