"""
Add misc variables to the page context.
"""

from __future__ import absolute_import, unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

from feincms import extensions, settings


class Extension(extensions.Extension):
    def handle_model(self):
        self.model.add_to_class('page_vars', models.ManyToManyField(
            settings.FEINCMS_DEFAULT_PAGE_MODEL,
            blank=True,
            null=True,
            related_name='%(app_label)s_%(class)s_related',
            help_text=_(
                'Add vars to the page in KEY:VAL format')))

    def handle_modeladmin(self, modeladmin):
        modeladmin.extend_list('filter_horizontal', ['related_pages'])

        modeladmin.add_extension_options(_('Related pages'), {
            'fields': ('related_pages',),
            'classes': ('collapse',),
        })