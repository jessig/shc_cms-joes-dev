from __future__ import absolute_import, unicode_literals
from django.core.wsgi import get_wsgi_application
import sys
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cms_app.settings")
sys.path.append('/webapps/django/shc_cms/lib')

application = get_wsgi_application()
