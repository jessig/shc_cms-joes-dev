# Django settings for box project.

from __future__ import absolute_import, unicode_literals

import os
import sys

ABS_PATH = os.path.dirname(os.path.abspath(__file__))

APP_BASEDIR = os.path.dirname(ABS_PATH)
APP_MODULE = ABS_PATH.split('/')[-1]

sys.path.insert(0, os.path.join(APP_BASEDIR, "lib"))

DEFAULT_FROM_EMAIL = SERVER_EMAIL = 'noreply@shc.edu'
DATE_FORMAT = 'm.d.Y'

from .settings_local import DEBUG
from .settings_local import MEDIA_URL

#DEBUG = 'runserver' in sys.argv
TEMPLATE_DEBUG = DEBUG
TOOLBAR = DEBUG

ADMINS = (
    ('SHC Web Dev', 'chughes@shc.edu'),
    ('SHC Web Dev', 'sublimevelo@gmail.com'),
)

MANAGERS = ADMINS

SITE_ID = 1

# USE_TZ = True
TIME_ZONE = 'US/Central'
LANGUAGE_CODE = 'en-us'

USE_I18N = True
USE_L10N = True

MEDIA_ROOT = os.path.join(APP_BASEDIR, 'media')

STATIC_ROOT = os.path.join(APP_BASEDIR, 'static')
STATIC_URL = '/static/'

if not DEBUG:
    TEMPLATE_LOADERS = (
        ('django.template.loaders.cached.Loader', (
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        )),
    )

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'compressor.finders.CompressorFinder',
)

STATICFILES_DIRS = (
    # this static folder is not in the root level of an app,
    # so needs to be added manually
    # os.path.join(APP_BASEDIR, "cms_theme/static"),
)

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # 'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'box.middleware.ForceDomainMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'cms_app.middleware.SHCRedirectFallbackMiddleware',
    # 'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
    'htmlmin.middleware.MarkRequestMiddleware'
)

TEMPLATE_CONTEXT_PROCESSORS = (
    # 'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.static',

    'django.contrib.auth.context_processors.auth',

    'feincms.context_processors.add_page_if_missing',
    'cms_app.context_processors.box',
    'cms_app.context_processors.menu_links',
    'cms_app.context_processors.page_vars',

    'django.contrib.messages.context_processors.messages',
)

ROOT_URLCONF = 'cms_app.urls'

# TEMPLATE_DIRS = (
#     '/home/chughes/webapps/django/shc_cms/cms_theme/templates',
#     os.path.join(APP_BASEDIR, APP_MODULE, 'templates'),
# )

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',

    'django.contrib.staticfiles',

    'django.contrib.sites',
    'django.contrib.sitemaps',

    'django.contrib.redirects',

    'adminsortable',
    'adminsortable2',

    'cms_theme',
    'cms_app',
    'cms_app.cms',

    'django_markup',

    'towel_foundation',
    'towel',

    'feincms',
    'feincms.module.medialibrary',
    'feincms.module.page',
    'feincms_links',
    'elephantblog',

    'magazine',
    'profiles',
    'gallery',

    'mptt',
    'crispy_forms',
    'forms_builder.forms',
    'forms_builder_integration',

    # 'south',
    'compressor',

    'storages',
    'easy_thumbnails',
    # 'django_thumbor',

    'django.contrib.admin',

    'ckeditor',  # for use OUTSIDE FeinCMS

    'degrees',
    'testimonials',
    'page_partials',
    'jobs',
    'home_features',


    'apps',

    'general',

    'clear_cache',

    'robots',

    # 'django_extensions',
)

LANGUAGES = (
    ('en', 'English'),
    # ('de', 'German'),
    # ('fr', 'French'),
    # ('it', 'Italian'),
)

MIGRATION_MODULES = dict((app, 'cms_app.migrate.%s' % app) for app in (
    'page',
    #'medialibrary',
    'elephantblog',
))

FEINCMS_ADMIN_MEDIA = '/static/'

# FEINCMS_RICHTEXT_INIT_CONTEXT = {
#     'TINYMCE_JS_URL': '/static/vendor/tinymce/tinymce.min.js',
#     # 'TINYMCE_CONTENT_CSS_URL': None,  # add your css path here
#     # 'TINYMCE_LINK_LIST_URL': None  # add your linklist.js path here
# }
# FEINCMS_RICHTEXT_INIT_TEMPLATE = 'admin/content/richtext/init_tinymce4.html'

FEINCMS_RICHTEXT_INIT_TEMPLATE = 'admin/content/richtext/init_ckeditor.html'
FEINCMS_RICHTEXT_INIT_CONTEXT = {
    'CKEDITOR_JS_URL': '/static/theme/vendor/ckeditor/ckeditor.js',
}

MAGAZINE_STORY_CAT_IDS = [
    11,
]

THUMBNAIL_ALIASES = {
    '': {
        'testimonial': {'size': (150, 100), 'crop': True},
    },
}

# Note that the CKEDITOR_* settings are for the stand-alone
# editor NOT the one loaded be FeinCMS!!
CKEDITOR_UPLOAD_PATH = 'content/ckeditor/'
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
# CKEDITOR_CONFIGS = {
#     'default': {
#         # 'customConfig': '/static/ckeditor/config.js',
#         'toolbar': 'Custom',
#         'toolbar_Custom': [
#             ['Bold', 'Italic', 'Underline'],
#             ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
#             ['Link', 'Unlink'],
#             ['RemoveFormat', 'Source']
#         ]
#     },
# }

CKEDITOR_CONFIGS = {
    'default': {
        'skin': 'moono',
        # 'skin': 'office2013',
        'toolbar_Basic': [
            ['Source', '-', 'Bold', 'Italic']
        ],
        'toolbar_YouCustomToolbarConfig': [
            {'name': 'styles', 'items': ['Styles', 'Format']},
            {'name': 'basicstyles',
             'items': ['Bold', 'Italic', '-', 'RemoveFormat']},
            {'name': 'paragraph',
             'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-',
                       'JustifyLeft', 'JustifyCenter']},
            {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
            '/',
            {'name': 'insert',
             'items': ['Image', 'Table', 'HorizontalRule', 'SpecialChar']},
            {'name': 'clipboard', 'items': [
                'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            {'name': 'editing', 'items': [
                'Find', 'Replace', '-', 'SelectAll']},
            # {'name': 'forms',
            #  'items': ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
            #            'HiddenField']},
            '/',
            # {'name': 'colors', 'items': ['TextColor', 'BGColor']},
            {'name': 'document', 'items': ['Source']},
            {'name': 'tools', 'items': ['Maximize', 'ShowBlocks', 'Preview']},
            {'name': 'about', 'items': ['About']},
            #'/',  # put this to force next toolbar on new line
            # {'name': 'youcustomtools', 'items': [
            #     # put the name of your editor.ui.addButton here
            #     'Preview',
            #     'Maximize',

            # ]},
        ],
        'toolbar': 'YouCustomToolbarConfig',  # put selected toolbar config here
        # 'toolbarGroups': [{ 'name': 'document', 'groups': [ 'mode', 'document', 'doctools' ] }],
        # 'height': 291,
        # 'width': '100%',
        # 'filebrowserWindowHeight': 725,
        # 'filebrowserWindowWidth': 940,
        # 'toolbarCanCollapse': True,
        # 'mathJaxLib': '//cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML',
        'tabSpaces': 4,
        'extraPlugins': ','.join(
            [
                # you extra plugins here
                'div',
                'autolink',
                'autoembed',
                'embedsemantic',
                'autogrow',
                # 'devtools',
                'widget',
                'lineutils',
                'clipboard',
                'dialog',
                'dialogui',
                'elementspath'
            ]),
    }
}

LOCALE_PATHS = (
    os.path.join(APP_BASEDIR, 'conf', 'locale'),
)

SHC_CMS = {
    "image_paths": {
        "academic_area_header_image_pattern": "https://s3.amazonaws.com/shc.edu.cms.static/images/academic-area-headers/%s",
    },
    "image_thumbs": {
        "thumb": {
            "size": (120, 0),
            "width": 120,
            "height": 0,
            "crop": True
        },
        "thumb_sq": {
            "size": (120, 120),
            "width": 120,
            "height": 120,
            "crop": True
        },
        "mid": {
            "size": (300, 0),
            "width": 300,
            "height": 0,
            "crop": True
        },
        "full": {
            "size": (660, 0),
            "width": 660,
            "height": 0,
            "crop": True
        }
    }
}

# # The host serving the thumbor resized images
# THUMBOR_SERVER = 'http://tn.shc.edu'
# # The prefix for the host serving the original images
# # This must be a resolvable address to allow thumbor to reach the images
# # THUMBOR_MEDIA_URL = 'https://s3.amazonaws.com/shc.edu.cms.static'
# THUMBOR_MEDIA_URL = 'http://www.shc.edu/media'
# # THUMBOR_SECURITY_KEY in settings_local


# # S3 makes compression a lot more difficult
# # Use NGINX for static file serving and S3 behind thumbor for media
# DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
# # STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
# # AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in settings_local
# AWS_STORAGE_BUCKET_NAME = "shc.edu.cms.media"
# from boto.s3.connection import OrdinaryCallingFormat
# AWS_S3_CALLING_FORMAT = OrdinaryCallingFormat()

BLOG_TITLE = "Spring Hill College News"
BLOG_DESCRIPTION = "The lastest news and events from The Hill"

OG_IMAGE_DEFAULT = '%simages/common/spring-hill-college-avenue-azaleas.jpg' % MEDIA_URL

# map story categories to page partial slugs
MEDIA_CONTACT_MAP = {
    "general": "media-contact-general",
    "athletics": "media-contact-athletics",
    "__default__": "media-contact-general",
}


def elephantblog_entry_url_app(self):
    from feincms.content.application.models import app_reverse
    return app_reverse('elephantblog_entry_detail', 'elephantblog.urls', kwargs={
        'year': self.published_on.strftime('%Y'),
        'month': self.published_on.strftime('%m'),
        'day': self.published_on.strftime('%d'),
        'slug': self.slug,
    })


def elephantblog_categorytranslation_url_app(self):
    from feincms.content.application.models import app_reverse
    return app_reverse('elephantblog_category_detail', 'elephantblog.urls', kwargs={
        'slug': self.slug,
    })

ABSOLUTE_URL_OVERRIDES = {
    'elephantblog.entry': elephantblog_entry_url_app,
    'elephantblog.categorytranslation': elephantblog_categorytranslation_url_app,
}

# CACHES is overridden in settings_local for dev and staging machines
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/django_cache',
    }
}
CACHE_MIDDLEWARE_ALIAS = "default"
CACHE_MIDDLEWARE_SECONDS = 300
CACHE_MIDDLEWARE_KEY_PREFIX = "shc_cms"

###################################################

# import ldap
# from django_auth_ldap.config import LDAPSearch, ActiveDirectoryGroupType

# # Baseline configuration.
# AUTH_LDAP_SERVER_URI = "ldap://its-server2.shc.edu:389"

# AUTH_LDAP_BIND_DN = "CN=chughes,CN=Users,DC=shc,DC=edu"
# AUTH_LDAP_BIND_PASSWORD = "myDucati916"
# # AUTH_LDAP_USER_SEARCH = LDAPSearch("CN=Users,DC=shc,DC=edu",
# #     ldap.SCOPE_SUBTREE, "(sAMAccountName=%(user)s)")
# # or perhaps:
# AUTH_LDAP_USER_DN_TEMPLATE = "CN=%(user)s,CN=Users,DC=shc,DC=edu"

# # # Set up the basic group parameters.
# AUTH_LDAP_GROUP_SEARCH = LDAPSearch("CN=Users,DC=shc,DC=edu",
#     ldap.SCOPE_SUBTREE, "(objectClass=group)"
# )
# AUTH_LDAP_GROUP_TYPE = ActiveDirectoryGroupType()

# # Simple group restrictions
# # AUTH_LDAP_REQUIRE_GROUP = "CN=G.Web-CMS-Users,CN=Users,DC=shc,DC=edu"
# # AUTH_LDAP_DENY_GROUP = "cn=disabled,ou=django,ou=groups,dc=example,dc=com"

# # Populate the Django user from the LDAP directory.
# AUTH_LDAP_USER_ATTR_MAP = {
#     "email": "mail",
#     "first_name": "givenName",
#     "last_name": "sn",
# }

# # AUTH_LDAP_PROFILE_ATTR_MAP = {
# #     "employee_number": "employeeNumber"
# # }

# AUTH_LDAP_USER_FLAGS_BY_GROUP = {
#     "is_active": "CN=G.Web-Admins,CN=Users,DC=shc,DC=edu",
#     "is_staff": "CN=G.Web-Admins,CN=Users,DC=shc,DC=edu",
#     "is_superuser": "CN=G.Web-Admins,CN=Users,DC=shc,DC=edu"
# }

# # AUTH_LDAP_PROFILE_FLAGS_BY_GROUP = {
# #     "is_awesome": "cn=awesome,ou=django,ou=groups,dc=example,dc=com",
# # }

# # Use LDAP group membership to calculate group permissions.
# AUTH_LDAP_FIND_GROUP_PERMS = True

# # Cache group memberships for an hour to minimize LDAP traffic
# AUTH_LDAP_CACHE_GROUPS = True
# AUTH_LDAP_GROUP_CACHE_TIMEOUT = 7200

# Keep ModelBackend around for per-user permissions and maybe a local
# superuser.
AUTHENTICATION_BACKENDS = (
    # 'django_auth_ldap.backend.LDAPBackend',
    'django.contrib.auth.backends.ModelBackend',
)

ALLOWED_HOSTS = ['*']

###################################################

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['console'],
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        # Root handler.
        '': {
            'handlers': ['console'],
        },
        'django.request': {
            'handlers': ['console'],
        },
    },
}

if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
    LOGGING['loggers'].update({
        'django.db.backends': {
            'level': 'DEBUG',
            # 'handlers': ['console'],  # Uncomment to dump SQL statements.
            'propagate': False,
        },
        'django.request': {
            'level': DEBUG,
            'handlers': ['console'],  # Dump exceptions to the console.
            'propagate': False,
        },
    })
    if TOOLBAR:
        INSTALLED_APPS += (
            'debug_toolbar',  # Django 1.7: debug_toolbar.apps.DebugToolbarConfig
        )
        INTERNAL_IPS = ('127.0.0.1', '192.168.10.1',
                        '10.21.20.68', '52.3.249.36')
        MIDDLEWARE_CLASSES = (
            'debug_toolbar.middleware.DebugToolbarMiddleware',
        ) + MIDDLEWARE_CLASSES
        DEBUG_TOOLBAR_PATCH_SETTINGS = False

from .settings_local import *
