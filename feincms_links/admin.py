from django import forms
from django.conf import settings
from django.contrib import admin

from .models import Category, Link


class LinkAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'description', 'url',)
    search_fields = ('name', 'description', 'url',)
    list_filter = ('categories',)


class LinkOrderForm(forms.ModelForm):
    class Media:
        model = Link
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js',
            'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js',
            settings.STATIC_URL + 'shc_links/move-order.js',
        )


class LinkAdminInline(admin.StackedInline):
    model = Link.categories.through
    form = LinkOrderForm
    extra = 1


class LinkCategoryAdmin(admin.ModelAdmin):
    model = Category
    inlines = [LinkAdminInline]


admin.site.register(Link, LinkAdmin)
admin.site.register(Category, LinkCategoryAdmin)
# admin.site.register(Category)
