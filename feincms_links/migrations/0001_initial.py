# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('description', models.CharField(max_length=200, verbose_name='description', blank=True)),
                ('ordering', models.PositiveIntegerField(default=0, verbose_name='ordering')),
            ],
            options={
                'ordering': ('ordering', 'name'),
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('description', models.CharField(max_length=200, verbose_name='description', blank=True)),
                ('url', models.URLField(verbose_name='URL')),
                ('ordering', models.PositiveIntegerField(default=0, verbose_name='ordering')),
                ('category', models.ForeignKey(verbose_name='category', to='feincms_links.Category')),
            ],
            options={
                'ordering': ('ordering', 'name'),
                'verbose_name': 'link',
                'verbose_name_plural': 'links',
            },
            bases=(models.Model,),
        ),
    ]
