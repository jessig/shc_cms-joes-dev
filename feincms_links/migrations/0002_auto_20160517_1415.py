# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feincms_links', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='link',
            name='category',
        ),
        migrations.AddField(
            model_name='link',
            name='categories',
            field=models.ManyToManyField(to='feincms_links.Category', verbose_name='category'),
            preserve_default=True,
        ),
    ]
