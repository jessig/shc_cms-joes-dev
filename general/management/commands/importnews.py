from django.core.management.base import LabelCommand
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from feincms.content.richtext.models import RichTextContent
import sys
import csv
from elephantblog.models import Entry, Category
from datetime import datetime


class Command(LabelCommand):

    help = "Dumps data into the news database from the given CSV file"
    args = "[csv file path]"

    def handle_label(self, label, **options):

        csv_path = label

        fh = open(csv_path, 'r')
        reader = csv.reader(fh)
        headers = reader.next()

        default_author = User.objects.get(pk=17)

        # kill existing entries FOR NOW!!!!
        Entry.objects.all().delete()

        ######
        # Process newsfeed
        ######

        newsfeed = []
        EntryRichTextContent = Entry.content_type_for(RichTextContent)

        # Grab newsfeed from CSV
        for row in reader:
            newsfeed.append(dict(zip(headers, row)))

        # Build and save newsfeed
        for row in newsfeed:
            entry = Entry()

            entry.is_featured = bool(row["sticky"])
            entry.title = row["title"].strip()[:100]

            # create and add slug
            entry.slug = slugify(row["title"].strip()[:50])

            # create and add author
            entry.author = default_author

            def ts(ts):
                dt = datetime.fromtimestamp(ts)
                return dt.strftime("%Y-%m-%d %H:%M")

            # convert and add timestamps
            entry.published_on = ts(float(row["created"]))
            entry.last_changed = ts(float(row["changed"]))

            print "SAVING ",
            print "'%s'" % (entry.title),
            # must save before we can get a pk
            entry.save()
            print entry.pk

            content = EntryRichTextContent(
                text=row["body"],
                parent_id=entry.pk,
                region="main",
                ordering=0
            )
            content.save()

            print "."
            sys.stdout.flush()

        print
        print "Processed %s Entries" % len(newsfeed)
