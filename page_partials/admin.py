from django.contrib import admin
from .models import PagePartial
from django.conf import settings


class PagePartialAdmin(admin.ModelAdmin):
    ordering = ("admin_title",)
    prepopulated_fields = {"slug": ("admin_title",)}

    class Media:
        js = (
            settings.CKEDITOR_JQUERY_URL,
            "theme/scripts/admin/textile.js",
        )
        css = {
            "all": ("theme/styles/admin/textile.css",),
        }

# Register your models here.
admin.site.register(PagePartial, PagePartialAdmin)
