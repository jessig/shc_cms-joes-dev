from django.db import models
# from cms_app.cms.textile_enhance import enhance_textile
import textile
# Create your models here.


class PagePartial(models.Model):
    admin_title = models.CharField(
        max_length=100
    )
    slug = models.SlugField(
        blank=True
    )
    content = models.TextField()

    def get_rendered_content(self):
        return textile.textile(self.content)

    def __unicode__(self):
        return "%s" % self.admin_title
