from django.db import models
import textile

# Create your models here.
PERSON_CLASS_CHOICES = (
    ("fac", "Faculty"),
    ("sta", "Staff"),
    ("stu", "Student"),
    ("alum", "Alumni")
)


class Testimonial(models.Model):
    person_class = models.CharField(
        max_length=10, choices=PERSON_CLASS_CHOICES)
    title = models.CharField(max_length=100, blank=True)
    attribution = models.CharField(max_length=100, blank=True)
    image = models.ImageField(blank=True, null=True, upload_to="images/people")

    content = models.TextField()

    def __unicode__(self):
        return self.attribution

    def rendered_content(self, head_offset=1):
        return textile.textile(self.content, head_offset=head_offset)
