from django.contrib import admin
from .models import Testimonial
# Register your models here.

class TestimonialAdmin(admin.ModelAdmin):
    list_display = ('attribution', 'person_class', 'title')
    list_filter = ('person_class',)
    ordering = ('attribution',)
    search_fields = ('attribution',)

admin.site.register(Testimonial, TestimonialAdmin)
