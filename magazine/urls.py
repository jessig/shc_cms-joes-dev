from django.conf.urls import patterns, url
# from .views import MagazineIndex, MagazineHome, MagazineDetail, MagazineStoryDetail
from .views import magazine_index, magazine_home, magazine_detail, magazine_story

urlpatterns = patterns('',
                       url(r'^$',
                           magazine_home, name='magazine_home'),
                       url(r'^index/?$', magazine_index,
                           name='magazine_index'),
                       url(r'^(?P<slug>[-\w]+)/$',
                           magazine_detail, name='magazine_detail'),
                       url(r'^(?P<slug>[-\w]+)/(?P<pk>[0-9]+)/(?P<story_slug>[-\w]+)/$',
                           magazine_story, name='magazine_story_detail')
                       )
