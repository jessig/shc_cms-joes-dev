# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import feincms.module.medialibrary.fields


class Migration(migrations.Migration):

    dependencies = [
        ('elephantblog', '0001_initial'),
        ('medialibrary', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='MagazineIssue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('active', models.BooleanField()),
                ('title', models.CharField(max_length=100)),
                ('year', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1997), django.core.validators.MaxValueValidator(2037)])),
                ('issue', models.PositiveIntegerField(default=1, choices=[(1, b'Fall'), (2, b'Winter'), (3, b'Spring'), (4, b'Summer')])),
                ('slug', models.SlugField()),
                ('issuu_link', models.URLField(null=True, blank=True)),
                ('banner_image', feincms.module.medialibrary.fields.MediaFileForeignKey(related_name='+', blank=True, to='medialibrary.MediaFile', null=True)),
            ],
            options={
                'ordering': ['-year', '-issue'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MagazineStory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('story_type', models.CharField(max_length=10, choices=[(b'pres', b'Message from the President'), (b'cover', b'Cover Story'), (b'feature', b'Features'), (b'news', b'News'), (b'faculty', b'Faculty Updates'), (b'jesuit', b'Jesuit Perspective')])),
                ('teaser', models.TextField(help_text=b'Only shows up on issue landing pages', max_length=1000, blank=True)),
                ('order', models.PositiveIntegerField(default=0, editable=False, db_index=True)),
                ('issue', models.ForeignKey(to='magazine.MagazineIssue')),
                ('lead_image', feincms.module.medialibrary.fields.MediaFileForeignKey(related_name='+', blank=True, to='medialibrary.MediaFile', help_text=b'Only shows up on issue landing pages', null=True)),
                ('story', models.ForeignKey(to='elephantblog.Entry')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name_plural': 'magazine stories',
            },
            bases=(models.Model,),
        ),
    ]
