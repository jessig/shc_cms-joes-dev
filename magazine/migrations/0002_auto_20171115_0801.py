# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from magazine.models import MagazineIssue

def update_issues(apps, schema_editor):
    issues = MagazineIssue.objects.all()
    for issue in issues:
        if issue.issue == 2:
            issue.issue = 3
            issue.save()
        else:
            pass


class Migration(migrations.Migration):

    dependencies = [
        ('magazine', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='magazineissue',
            name='issue',
            field=models.PositiveIntegerField(default=1, choices=[(1, b'Fall'), (2, b'Fall/Winter'), (3, b'Winter'), (4, b'Winter/Spring'), (5, b'Spring'), (6, b'Spring/Summer'), (7, b'Summer'), (8, b'Summer/Fall')]),
            preserve_default=True,
        ),
        migrations.RunPython(update_issues),
    ]
