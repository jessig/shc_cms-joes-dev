from django.shortcuts import render
from django.http import Http404
from django.views.generic import TemplateView, ListView, DetailView

from elephantblog.models import Entry
from elephantblog.utils import entry_list_lookup_related

try:
    from feincms.contents import RichTextContent
    from feincms.module.medialibrary.contents import MediaFileContent
except ImportError:  # FeinCMS<2
    from feincms.content.richtext.models import RichTextContent
    from feincms.content.medialibrary.models import MediaFileContent

from .models import MagazineIssue, MagazineStory, STORY_TYPE_CHOICES


def get_story_list(magazine_issue):
    stories = MagazineStory.objects.filter(issue__slug=magazine_issue.slug)
    returns = []
    for story in stories:
        try:
            story.story.first_image = [
                mediafile
                for mediafile in story.story.content.all_of_type(
                    MediaFileContent)
                if mediafile.mediafile.type == 'image'][0]
        except IndexError:
            pass
        try:
            story.story.first_richtext = story.story.content.all_of_type(
                RichTextContent)[0]
        except IndexError:
            pass
        returns.append(story)
    return returns


def get_issue_list(magazine_issue=None, show_inactive=True, num=0):
    if show_inactive:
        issues = MagazineIssue.all_issues.all()
    else:
        issues = MagazineIssue.objects.all()
    count = issues.count()
    if magazine_issue:
        issues = issues.exclude(slug=magazine_issue.slug)
    if num:
        issues = issues[:num]
    return {'issues': issues, 'more': count > num}


def transform_magazine_context(request, context):
    show_inactive = request.user.is_staff
    issue = None
    if context.get('magazine', False):
        issue = context['magazine']
    if context.get('story', False):
        issue = context['story'].issue
    context['issue'] = issue
    context['story_groups'] = dict(STORY_TYPE_CHOICES)
    context['stories'] = get_story_list(issue)
    issues = get_issue_list(issue, show_inactive, num=5)
    context['magazine_issues'] = issues['issues']
    if issues['more']:
        context['more_issues'] = issues['more']
    return context


def magazine_home(request):
    show_inactive = request.user.is_staff
    context = {}
    if show_inactive:
        magazine = MagazineIssue.all_issues.all()[0]
    else:
        magazine = MagazineIssue.objects.all()[0]
    context['magazine'] = magazine
    if magazine:
        context = transform_magazine_context(request, context)
    return 'magazine/magazineissue_detail.html', context


def magazine_index(request):
    show_inactive = request.user.is_staff
    context = {'magazines': get_issue_list(show_inactive=show_inactive
                                           )['issues']}
    return 'magazine/magazineissue_list.html', context


def magazine_detail(request, slug):
    show_inactive = request.user.is_staff
    if show_inactive:
        magazine = MagazineIssue.all_issues.get(slug=slug)
    else:
        magazine = MagazineIssue.objects.get(slug=slug)
    context = {'magazine': magazine}
    context = transform_magazine_context(request, context)
    return 'magazine/magazineissue_detail.html', context


def magazine_story(request, slug, pk, story_slug):
    show_inactive = request.user.is_staff
    story = MagazineStory.objects.get(pk=pk)
    if not story.issue.active and not show_inactive:
        raise Http404("This story is not available")
    context = {'story': story}
    context = transform_magazine_context(request, context)
    return 'magazine/magazinestory_detail.html', context


# class MagazineDetail(DetailView):
#     model = MagazineIssue
#     context_object_name = 'magazine'
#
#     def get_context_data(self, **kwargs):
#         context = super(MagazineDetail, self).get_context_data(**kwargs)
#         return transform_magazine_context(context['object'], context)
#
#
# class MagazineIndex(ListView):
#     model = MagazineIssue
#     context_object_name = 'magazines'
#
#     def get_context_data(self, **kwargs):
#         context = super(MagazineIndex, self).get_context_data(**kwargs)
#         context['magazine_issues'] = get_issue_list()['issues']
#         return context
#
#
# class MagazineHome(MagazineDetail):
#
#     def get_object(self):
#         return MagazineIssue.objects.all()[0]
#
#     def get_context_data(self, **kwargs):
#         context = super(MagazineDetail, self).get_context_data(**kwargs)
#         return transform_magazine_context(context['object'], context)
#
#
# class MagazineStoryDetail(TemplateView):
#     template_name = 'magazine/magazinestory_detail.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(MagazineStoryDetail, self).get_context_data(**kwargs)
#         pk = context['pk']
#         context['story'] = MagazineStory.objects.get(pk=pk)
#         return transform_magazine_context(context['story'].issue, context)
