from django.contrib import admin
from adminsortable.admin import NonSortableParentAdmin, SortableTabularInline
from .models import MagazineIssue, MagazineStory


class StoryInline(SortableTabularInline):
    model = MagazineStory
    raw_id_fields = ('lead_image', 'story', )


@admin.register(MagazineIssue)
class MagazineIssueAdmin(NonSortableParentAdmin):
    raw_id_fields = ('banner_image', )
    prepopulated_fields = {'slug': ('year', 'issue', )}
    inlines = [
        StoryInline,
    ]
