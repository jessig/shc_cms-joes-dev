from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from django.utils.text import slugify

from feincms.module.medialibrary.models import MediaFile, Category
from feincms.module.medialibrary.fields import MediaFileForeignKey

from adminsortable.models import SortableMixin

from elephantblog.models import Entry

import datetime

ISSUE_CHOICES = (
    (1, 'Fall'),
    (2,'Fall/Winter'),
    (3, 'Winter'),
    (4,'Winter/Spring'),
    (5, 'Spring'),
    (6,'Spring/Summer'),
    (7, 'Summer'),
    (8,'Summer/Fall')
)

STORY_TYPE_CHOICES = (
    ('pres', 'Message from the President'),
    ('cover', 'Cover Story'),
    ('feature', 'Features'),
    ('news', 'News'),
    ('faculty', 'Faculty Updates'),
    ('jesuit', 'Jesuit Perspective')
)


class ActiveMagazineIssues(models.Manager):

    def get_query_set(self):
        return super(ActiveMagazineIssues, self).get_query_set().filter(
            active=True
        )


class MagazineIssue(models.Model):
    this_year = datetime.datetime.now().year
    active = models.BooleanField()
    title = models.CharField(
        max_length=100
    )
    year = models.PositiveIntegerField(
        validators=[
            MinValueValidator(this_year - 20),
            MaxValueValidator(this_year + 20)
        ]
    )
    issue = models.PositiveIntegerField(
        choices=ISSUE_CHOICES,
        default=1
    )
    slug = models.SlugField()
    banner_image = MediaFileForeignKey(
        MediaFile,
        related_name='+',
        limit_choices_to={'type': 'image'},
        blank=True,
        null=True
    )
    issuu_link = models.URLField(
        blank=True,
        null=True
    )
    # issuu_link_image = MediaFileForeignKey(
    #     MediaFile,
    #     related_name='+',
    #     limit_choices_to={'type': 'image'},
    #     blank=True,
    #     null=True
    # )

    all_issues = models.Manager()
    objects = ActiveMagazineIssues()

    class Meta:
        ordering = ['-year', '-issue']

    def __unicode__(self):
        return self.title

    @property
    def issue_string(self):
        return "%s %s" % (self.get_issue_display(), self.year)


class MagazineStory(SortableMixin):
    issue = models.ForeignKey(
        MagazineIssue
    )
    story = models.ForeignKey(
        Entry
    )
    story_type = models.CharField(
        max_length=10,
        choices=STORY_TYPE_CHOICES
    )
    lead_image = MediaFileForeignKey(
        MediaFile,
        related_name='+',
        blank=True,
        null=True,
        help_text="Only shows up on issue landing pages"
    )
    teaser = models.TextField(
        max_length=1000,
        help_text="Only shows up on issue landing pages",
        blank=True
    )
    order = models.PositiveIntegerField(
        default=0,
        editable=False,
        db_index=True
    )

    @property
    def listing_template(self):
        # print dir(self.lead_image)
        return "magazine/partials/%s_story.html" % self.story_type

    def __unicode__(self):
        return self.story.title

    class Meta:
        verbose_name_plural = "magazine stories"
        ordering = ['order', ]
