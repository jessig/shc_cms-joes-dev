from django.template.loader import (
    TemplateDoesNotExist, get_template, render_to_string)

from feincms.module.page.models import Page
from django.http import HttpResponse
from django.utils.safestring import mark_safe
from django.shortcuts import render_to_response


def render_template(request, pid, filename, **kwargs):
    context = kwargs.pop('context', {})

    page = Page.objects.get(pk=pid)
    context['feincms_page'] = page

    return render_to_response(filename, context)


def xhr_textile(request):
    from cms_app.cms.textile_enhance import enhance_textile
    content = request.GET.get('content', '')
    return HttpResponse(mark_safe(enhance_textile(content)))


def fake_iframe(request):
    head = request.GET.get('head', '')
    body = request.GET.get('body', '')
    html = ['<html><head>']
    html.append(head)
    html.append('</head><body>')
    html.append(body)
    html.append('</body></html>')
    return HttpResponse(''.join(html))
