$ ->
  LG_SETTINGS = {
    mode: 'lg-fade'
    pager: true
    speed: 1000
    easing: 'swing'
    }
  if $(".image-gallery").length
    $(".image-gallery").justifiedGallery({
      rowHeight : 200,
      lastRow : 'nojustify',
      margins : 5
    }).on('jg.complete', ->
      build_gallery()
      )

    build_gallery = () ->
      two_row_padded_height = 77
      two_row_overflow_height = 60
      lg = $(".image-gallery")
      lg.lightGallery(LG_SETTINGS)
      lg.on('onAfterOpen.lg', ->
        $(".lg-outer .lg-dropdown").prepend($('<li class="heading">Share this gallery:</li>'))
        )
      lg.on('onAfterAppendSubHtml.lg', ->
        self = $(this)
        full_captions = self.data('full-captions') or false
        sub = $(".lg-sub-html")
        if sub
          # reset classes and css, then get the natural height of the block
          sub.removeClass 'more active'
          sub.css('height', 'auto')
          h = sub.outerHeight() + 16

          # add classes and set height as needed
          if h > two_row_padded_height
            sub.addClass 'more'
            sub.css('height', "#{two_row_overflow_height}px")
          if full_captions
            sub.css('height', "#{h}px")
            sub.addClass('active')
          else
            sub.removeClass('active')

          sub.off 'click'
          sub.on 'click', ->
            full_captions = self.data('full-captions') or false
            sub.toggleClass 'active'
            if not full_captions
              sub.css('height', "#{h}px")
            else
              sub.css('height', "#{two_row_overflow_height}px")
            self.data('full-captions', !full_captions)
            $('.lg-pager-outer').toggleClass('lg-pager-hidden')
        )
