textile_getter = "textarea[name^='textilepagecontent'], .app-page_partials .vLargeTextField"

set_textile = (me) ->
    $.get '/textile', {content: me.val()}, (data) ->
        me.next 'div.textile-frame'
            .find 'iframe'
            .contents()
            .find '.rendered-textile'
            .html data

init_textile_render = ->
    textile_blocks = $(textile_getter)
    if textile_blocks.length
        textile_blocks.each ->
            self = $(this)
            # don't attach handlers to the 'fake' fields
            if /textilepagecontent_set-[0-9]+-content/.test self.attr("name") or not self.attr("name")
                self.closest('.form-row').css 'position', 'relative'
                if not self.siblings('label.render-textile').length
                    cb = $('<label class="render-textile"><input type="checkbox"></input> render txt?</label>')
                        .prependTo(self.closest('div'))
                        .find('input')
                        .on 'click', ->
                            render_textile($(this).is(':checked'))

render_textile = (is_checked) ->
    textile_areas = $(textile_getter)
    if is_checked
        store.setItem('trender', "1")
        $('label.render-textile input')
            .prop 'checked', true
        textile_areas.each () ->
            self = $(this)
            if not self.siblings('div.textile-frame').length
                head = encodeURIComponent('<link href="/static/theme/styles/theme.min.css" rel="stylesheet" type="text/css">')
                body = encodeURIComponent('<div class="rendered-textile"></div>')
                self.after "<div class=\"textile-frame\"><iframe src=\"/fake_iframe?head=#{head}&body=#{body}\"></iframe></div>"
                self.next('.textile-frame')
                    .find 'iframe'
                    .on 'load', ->
                        set_textile(self)
            else
                self.siblings('.textile-frame').show()
                set_textile(self)
            self.on 'keyup', ->
                self.siblings('.textile-frame').addClass 'working'
                window.clearTimeout window.render_timer
                window.render_timer = window.setTimeout ->
                    self.siblings('.textile-frame').removeClass 'working'
                    set_textile(self)
                , 500
    else
        store.setItem('trender', "0")
        $('label.render-textile input').prop 'checked', false
        textile_areas.each ->
            $(this).siblings('.textile-frame').hide()
            # unbind keyup to avoid uneccesary trips to server
            $(this).off 'keyup'

window.store = localStorage
window.render_timer = null

kickoff = ->
    init_textile_render()
    if store.getItem('trender') is "1"
        $('label.render-textile input').prop 'checked', true
        render_textile true

contentblock_init_handlers.push kickoff
