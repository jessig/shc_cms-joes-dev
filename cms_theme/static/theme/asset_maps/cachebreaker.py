import re
import json

theme_base = "/webapps/django/shc_cms/cms_theme"
asset_map = "%s/static/theme/asset_maps/versioning.map" % theme_base
infile = "%s/templates/base.src.html" % theme_base
outfile = "%s/templates/base.html" % theme_base

in_fh = open(infile, "r")

outs = []

for line in in_fh.readlines():
    for item in json.loads(open(asset_map, 'r').read()):
        # find source and versioned filenames
        src = item["originalPath"].split("/")[-1]
        dest = item["versionedPath"].split("/")[-1]
        # replace source filename with versioned
        line = re.sub(src, dest, line)
    outs.append(line)
in_fh.close()

fh = open(outfile, "w")
fh.writelines(outs)
fh.close()
