# /**
#  * Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
#  * For licensing, see LICENSE.md or http://ckeditor.com/license
#  */

# // This file contains style definitions that can be used by CKEditor plugins.
# //
# // The most common use for it is the "stylescombo" plugin, which shows a combo
# // in the editor toolbar, containing all styles. Other plugins instead, like
# // the div plugin, use a subset of the styles on their feature.
# //
# // If you don't have plugins that depend on this file, you can simply ignore it.
# // Otherwise it is strongly recommended to customize this file to match your
# // website requirements and design properly.

STYLES = [
    # /* Block Styles */

    {
        name: 'Inline list'
        element: 'ul'
        attributes:
            'class': 'inline-list'
    }

    # /* Inline Styles */

    {
        name: 'Button'
        element: 'a'
        attributes:
            'class': 'button'
    }
    {
        name: 'Label'
        element: 'span'
        attributes:
            'class': 'label'
    }
    {
        name: 'Small'
        element: 'small'
    }
    {
        name: 'Inline Quotation'
        element: 'q'
    }

    # /* Object Styles */

    {
        name: 'Styled image (left)'
        element: 'img'
        attributes:
            'class': 'left'
    }
    {
        name: 'Styled image (right)'
        element: 'img'
        attributes:
            'class': 'right'
    }
]

VISIBILITY_CLASSES = [
    "show-for-small-only"
    "show-for-medium-up"
    "show-for-medium-only"
    "show-for-large-up"
    "show-for-large-only"
    "show-for-xlarge-up"
    "show-for-xlarge-only"
    "show-for-xxlarge-up"
]

cleanclass = (name) ->
    name.replace(/-/g, ' ')

for visclass in VISIBILITY_CLASSES
    s = {
        name: cleanclass visclass
        element: 'p'
        attributes:
            'class': visclass
    }
    STYLES.push(s)

CKEDITOR.stylesSet.add 'cms_styles', STYLES
