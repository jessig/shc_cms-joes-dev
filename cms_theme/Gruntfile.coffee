tryts = ->
    new Date().getTime()


module.exports = (grunt) ->

    venv_root = "/webapps/Envs/shc_cms/"

    banner = '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'

    # Project configuration.
    grunt.initConfig
        pkg: grunt.file.readJSON("package.json")

        dirs: {
            static_root: "static"
            static_theme_root: "<%= dirs.static_root %>/theme"
            foundation_root: "<%= dirs.static_theme_root %>/vendor/foundation"
            bourbon_root: "<%= dirs.static_theme_root %>/vendor/bourbon/app/assets/stylesheets"
        }

        coffee:
            scripts:
                options:
                    bare: true
                files:
                    "<%= dirs.static_theme_root %>/scripts/app.js": ["<%= dirs.static_theme_root %>/coffee/app.coffee"]
            admin:
                options:
                    bare: true
                files:
                    "<%= dirs.static_theme_root %>/scripts/admin/textile.js": ["<%= dirs.static_theme_root %>/admin/textile/coffee/*.coffee"]
                    "<%= dirs.static_theme_root %>/scripts/admin/inline.js": ["<%= dirs.static_theme_root %>/admin/inline/coffee/*.coffee"]
                    "<%= dirs.static_root %>/richtext/styles.js": ["<%= dirs.static_root %>/richtext/**/*.coffee"]
            gallery:
              options:
                bare: true
              files:
                "<%= dirs.static_theme_root %>/scripts/image-gallery.js": ["<%= dirs.static_theme_root %>/coffee/image-gallery.coffee"]
            apps:
                expand: true
                flatten: false
                cwd: "../apps/static/apps/coffee"
                src: ["**/*.coffee"]
                dest: "../apps/static/apps/scripts"
                ext: ".js"

        sass:
            options:
                loadPath: ['<%= dirs.static_theme_root %>/scss', '<%= dirs.foundation_root %>/scss', '<%= dirs.bourbon_root %>']
            theme:
                files:
                    "<%= dirs.static_theme_root %>/styles/theme.css": "<%= dirs.static_theme_root %>/scss/theme.scss"
                    "<%= dirs.static_theme_root %>/styles/ie9.css": "<%= dirs.static_theme_root %>/scss/ie9.scss"
                    "<%= dirs.static_theme_root %>/styles/feincms/tree_editor_fixes.css": "<%= dirs.static_theme_root %>/scss/feincms/tree_editor_fixes.scss"
            admin:
                files:
                    "<%= dirs.static_theme_root %>/styles/admin/inline.css": "<%= dirs.static_theme_root %>/admin/inline/scss/*.scss"
                    "<%= dirs.static_theme_root %>/styles/admin/textile.css": "<%= dirs.static_theme_root %>/admin/textile/scss/*.scss"
                    "<%= dirs.static_theme_root %>/styles/admin/general.css": "<%= dirs.static_theme_root %>/admin/general/scss/*.scss"
            # profiles:
            #     files:
            #         "../profiles/static/profiles/styles.css": "../profiles/static/profiles/styles.scss"
            gallery:
              files:
                "<%= dirs.static_theme_root %>/styles/lightgallery.css": "<%= dirs.static_theme_root %>/scss/lightgallery.scss"
            apps:
                expand: true
                flatten: false
                cwd: "../apps/static/apps/scss"
                src: ["**/*.scss"]
                dest: "../apps/static/apps/styles"
                ext: ".css"

        clean:
            build: 'html/build/'

        includes:
            files:
                options:
                    flatten: true,
                src: '*.html'
                dest: 'html/build/'
                cwd: 'html/templates/'

        # copy:
            # icons:
            #     expand: true
            #     cwd: "vendor/foundation-icons/"
            #     src: "foundation-icons.*"
            #     dest: "static/theme/styles"
            # styles:
            #     src: "styles/ie9.css"
            #     dest: "static/theme/styles/ie9.css"
            # admin_styles:
            #     expand: true
            #     nonull: true
            #     cwd: venv_root + "/lib/python2.7/site-packages/feincms/static/feincms"
            #     src: ["tree_editor.css"]
            #     dest: "styles/feincms"

        concat:
            lib:
                src: [
                    "<%= dirs.static_theme_root %>/vendor/jquery/dist/jquery.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.abide.js"
                    # "<%= dirs.foundation_root %>/js/foundation/foundation.accordian.js"
                    # "<%= dirs.foundation_root %>/js/foundation/foundation.alert.js"
                    # "<%= dirs.foundation_root %>/js/foundation/foundation.clearing.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.dropdown.js"
                    # "<%= dirs.foundation_root %>/js/foundation/foundation.equalizer.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.interchange.js"
                    # "<%= dirs.foundation_root %>/js/foundation/foundation.joyride.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.magellan.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.offcanvas.js"
                    # "<%= dirs.foundation_root %>/js/foundation/foundation.orbit.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.reveal.js"
                    # "<%= dirs.foundation_root %>/js/foundation/foundation.slider.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.tab.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.tooltip.js"
                    "<%= dirs.foundation_root %>/js/foundation/foundation.topbar.js"
                    "static/theme/vendor/handlebars/handlebars.runtime.js"
                    "static/theme/vendor/store.min.js"
                    "static/theme/vendor/slick/slick.min.js"
                ]
                dest: "<%= dirs.static_theme_root %>/scripts/lib.all.js"
            app:
                src: [
                    "static/theme/scripts/hb.js"
                    "static/theme/scripts/app.js"
                ]
                dest: "<%= dirs.static_theme_root %>/scripts/app.all.js"
            css:
                src: [
                    "<%= dirs.static_theme_root %>/styles/theme.css"
                    "<%= dirs.static_theme_root %>/styles/admin.css"
                ]
                dest: "<%= dirs.static_theme_root %>/styles/ckeditor.css"

        uglify:
            options:
                banner: banner
            lib:
                src: "<%= dirs.static_theme_root %>/scripts/lib.all.js"
                dest: "<%= dirs.static_theme_root %>/scripts/lib.min.js"
            app:
                src: "<%= dirs.static_theme_root %>/scripts/app.all.js"
                dest: "<%= dirs.static_theme_root %>/scripts/app.min.js"
            apps:
                src: "../apps/static/apps/scripts/hb.js"
                dest: "../apps/static/apps/scripts/hb.min.js"
            profiles:
                src: "../profiles/static/profiles/scripts.js"
                dest: "../profiles/static/profiles/scripts.min.js"
            gallery:
                src: "<%= dirs.static_theme_root %>/scripts/image-gallery.js"
                dest: "<%= dirs.static_theme_root %>/scripts/image-gallery.min.js"
            modernizr:
                src: "<%= dirs.static_theme_root %>/vendor/modernizr/modernizr.js"
                dest: "<%= dirs.static_theme_root %>/vendor/modernizr/modernizr.min.js"

        handlebars:
            theme:
                options:
                    namespace: "hbt"
                files:
                    "<%= dirs.static_theme_root %>/scripts/hb.js": "templates/partials/**/*.handlebars",
            apps:
                options:
                    namespace: "hbt_apps"
                files:
                    "../apps/static/apps/scripts/hb.js": "../apps/templates/apps/**/*.handlebars",


        cssmin:
            theme:
                src: "<%= dirs.static_theme_root %>/styles/theme.css"
                dest: "<%= dirs.static_theme_root %>/styles/theme.min.css"
            gallery:
                src: "<%= dirs.static_theme_root %>/styles/lightgallery.css"
                dest: "<%= dirs.static_theme_root %>/styles/lightgallery.min.css"

        # filerev:
        #     options:
        #         algorithm: 'md5'
        #         length: 8
        #     scripts:
        #         src: ['<%= dirs.static_theme_root %>/scripts/**/*.js']
        #         dest: '<%= dirs.static_theme_root %>/scripts/dist'

        # replace:
        #     cachebreaker_lib:
        #         # replace a cachebreaker placeholder with timestamp
        #         src: ['templates/base.src.html']
        #         dest: 'templates/base.html'
        #         replacements: [
        #             {
        #                 from: /\*cachebreaker_lib\*\.(js|css)/gmi
        #                 to: (matchedWord, index, fullText, regexMatches) ->
        #                     timeStamp = new Date().getTime()
        #                     return timeStamp + '.' + regexMatches[0]
        #             }
        #         ]
        #     cachebreaker_app:
        #         # replace a cachebreaker placeholder with timestamp
        #         src: ['templates/base.src.html']
        #         dest: 'templates/base.html'
        #         replacements: [
        #             {
        #                 from: /\*cachebreaker_app\*\.(js|css)/gmi
        #                 to: (matchedWord, index, fullText, regexMatches) ->
        #                     timeStamp = new Date().getTime()
        #                     return timeStamp + '.' + regexMatches[0]
        #             }
        #         ]

        assets_versioning:
            assets:
                options:
                    versionsMapFile: "<%= dirs.static_theme_root %>/asset_maps/versioning.map"
                files: [
                    {
                        dest: "<%= dirs.static_theme_root %>/scripts/lib.min.js"
                        src: "<%= dirs.static_theme_root %>/scripts/lib.min.js"
                    },
                    {
                        dest: "<%= dirs.static_theme_root %>/scripts/app.min.js"
                        src: "<%= dirs.static_theme_root %>/scripts/app.min.js"
                    },
                    {
                        src: "<%= dirs.static_theme_root %>/styles/theme.min.css"
                        dest: "<%= dirs.static_theme_root %>/styles/theme.min.css"
                    },
                    {
                        src: "<%= dirs.static_theme_root %>/vendor/modernizr/modernizr.min.js"
                        dest: "<%= dirs.static_theme_root %>/vendor/modernizr/modernizr.min.js"
                    }
                ]

        shell:
            cachebreaker:
                command: "cd <%= dirs.static_theme_root %>/asset_maps/ && python cachebreaker.py "

        # sprite:
        #   all:
        #     src: '<%= dirs.static_theme_root %>/images/icons/*',
        #     dest: '<%= dirs.static_theme_root %>/images/sprite.png',
        #     destCss: '<%= dirs.static_theme_root %>/scss/sprite.scss',
        #     'imgPath': '../images/sprite.png',
        #     'cssTemplate': '<%= dirs.static_theme_root %>/scss.template/scss.template.mustache'

        # NEED TO UPDATE WATCH TO ONLY RUN NEEDED TASKS
        watch:
            options:
                livereload: true # default port 35729
            sass:
                files: [
                    "<%= dirs.static_theme_root %>/scss/**/*.scss"
                    "../apps/static/apps/scss/**/*.scss"
                ]
                tasks: ["do-all-css", "versioning"]
            admin_sass:
                files: ["<%= dirs.static_theme_root %>/admin/**/*.scss"]
                tasks: ["sass:admin"]
        #     # icons:
        #     #     files: ["<%= dirs.static_theme_root %>/images/icons/*.png"]
        #     #     tasks: ["sprite"]
            templates:
                files: ['html/templates/**/*.html']
                tasks: ['clean','includes']
            index:
                files: ['templates/base.src.html']
                tasks: ["versioning"]
            coffee:
                files: [
                    "<%= dirs.static_root %>/**/*.coffee"
                    "../apps/static/apps/coffee/*.coffee"
                    "../apps/templates/apps/**/*.handlebars"
                    "templates/partials/**/*.handlebars"
                    "../profiles/static/profiles/**/*.js"
                ]
                tasks: ["do-app-js", "versioning"]
                options:
                    spawn: false

    # Load the plugins
    grunt.loadNpmTasks "grunt-contrib-coffee"
    grunt.loadNpmTasks "grunt-contrib-sass"
    grunt.loadNpmTasks "grunt-contrib-concat"
    grunt.loadNpmTasks "grunt-contrib-uglify"
    grunt.loadNpmTasks "grunt-contrib-handlebars"
    grunt.loadNpmTasks "grunt-contrib-copy"
    grunt.loadNpmTasks "grunt-contrib-cssmin"
    grunt.loadNpmTasks "grunt-contrib-htmlmin"
    grunt.loadNpmTasks "grunt-contrib-imagemin"
    grunt.loadNpmTasks "grunt-newer"
    grunt.loadNpmTasks "grunt-shell"
    grunt.loadNpmTasks "grunt-contrib-watch"
    grunt.loadNpmTasks "grunt-text-replace"
    # grunt.loadNpmTasks "grunt-cache-breaker"
    grunt.loadNpmTasks "grunt-spritesmith"
    grunt.loadNpmTasks "grunt-contrib-clean"
    grunt.loadNpmTasks "grunt-includes"
    grunt.loadNpmTasks "grunt-favicons"
    grunt.loadNpmTasks "grunt-assets-versioning"

    # Default task(s).
    grunt.registerTask "do-lib-js", ["concat:lib", "uglify:lib", "uglify:modernizr"]
    grunt.registerTask "do-app-js", ["coffee", "handlebars", "concat:app", "uglify:app", "uglify:apps", "uglify:profiles", "uglify:gallery"]
    grunt.registerTask "do-all-js", ["do-lib-js", "do-app-js"]

    #grunt.registerTask "do-lib-css", ["sass:lib", "cssmin:lib", "assets_versioning:lib"]
    grunt.registerTask "do-all-css", ["sass", "concat:css", "cssmin:theme", "cssmin:gallery"]

    grunt.registerTask "versioning", ["assets_versioning", "shell"]

    grunt.registerTask "build", ["do-all-css", "do-all-js", "versioning"]
