from django.db import models
from django import forms
from feincms.module.medialibrary.models import MediaFile
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

class Gallery(models.Model):
    title = models.CharField(
        max_length=30,
        help_text="Administrative title. Will not appear in the gallery."
        )
    images = models.ManyToManyField(MediaFile, through='GalleryMediaFile')

    def ordered_images(self):
        return self.images.all().order_by('gallerymediafile__ordering').select_related()

    def count_images(self):
        if not getattr(self, '_image_count', None):
            self._image_count = self.images.count()
        return self._image_count

    def verbose_images(self):
        count = self.count_images()
        return ungettext_lazy('%(count)d Image',
                              '%(count)d Images', count) % {'count': count }
    verbose_images.short_description = _('Image Count')

    class Meta:
        verbose_name = _('Gallery')
        verbose_name_plural = _('Galleries')

    def __unicode__(self):
        return self.title


class GalleryMediaFile(models.Model):
    gallery = models.ForeignKey(Gallery)
    mediafile = models.ForeignKey(
        MediaFile
        )
    caption = models.CharField(
                   _('Caption - leave blank to use caption from Media Library'),
                   blank=True,
                   null=True,
                   max_length=555,
                   default=None,
                   )
    ordering = models.PositiveIntegerField(
        _('ordering'),
        default=0,
        blank=False,
        null=False,
        )

    class Meta:
        ordering = ['ordering']
        verbose_name = 'Image for Gallery'
        verbose_name_plural = 'Images for Gallery'

    def save(self, *args, **kwargs):
        if not self.caption:
            try:
                self.caption = self.mediafile.translation.caption.strip()
            except:
                self.caption = ''
        super(GalleryMediaFile, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' %self.mediafile


class GalleryContent(models.Model):
    gallery = models.ForeignKey(Gallery,
        help_text=_('Choose a gallery to render here'),
        related_name='%(app_label)s_%(class)s_gallery')

    class Meta:
        abstract = True
        verbose_name = _('Image Gallery')
        verbose_name_plural = _('Image Galleries')

    @property
    def media(self):
        return forms.Media(
            css={
                'all':
                    ('theme/vendor/justifiedGallery/dist/css/justifiedGallery.min.css',
                     'theme/vendor/lightgallery/dist/css/lightgallery.min.css',
                     'theme/styles/lightgallery.css',
                     )
            },
            js=('theme/vendor/justifiedGallery/dist/js/jquery.justifiedGallery.min.js',
                'theme/vendor/lightgallery/dist/js/lightgallery.min.js',
                'theme/vendor/lg-share/dist/lg-share.min.js',
                'theme/vendor/lg-pager/dist/lg-pager.min.js',
                'theme/scripts/image-gallery.min.js'),
        )

    def render(self, **kwargs):
        items = self.gallery.ordered_images()
        # get the caption for each image
        for item in items:
            try:
                item.caption = item.gallerymediafile_set.get(mediafile_id=item.id, gallery_id=self.gallery.id).caption.strip()
            except:
                item.caption = ''
        return render_to_string('gallery/image-gallery.html', {
            'items': items
        })
