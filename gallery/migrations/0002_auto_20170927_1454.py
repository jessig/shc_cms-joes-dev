# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gallery',
            name='title',
            field=models.CharField(help_text=b'Administrative title. Will not appear in the gallery.', max_length=30),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='gallerymediafile',
            name='caption',
            field=models.CharField(default=None, max_length=555, null=True, verbose_name='Caption - leave blank to use caption from Media Library', blank=True),
            preserve_default=True,
        ),
    ]
