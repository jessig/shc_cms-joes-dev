# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('medialibrary', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=30)),
            ],
            options={
                'verbose_name': 'Gallery',
                'verbose_name_plural': 'Galleries',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GalleryMediaFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('caption', models.CharField(default=None, max_length=555, null=True, verbose_name='Caption. Leave blank to use caption from Media Library', blank=True)),
                ('ordering', models.PositiveIntegerField(default=0, verbose_name='ordering')),
                ('gallery', models.ForeignKey(to='gallery.Gallery')),
                ('mediafile', models.ForeignKey(to='medialibrary.MediaFile')),
            ],
            options={
                'ordering': ['ordering'],
                'verbose_name': 'Image for Gallery',
                'verbose_name_plural': 'Images for Gallery',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='gallery',
            name='images',
            field=models.ManyToManyField(to='medialibrary.MediaFile', through='gallery.GalleryMediaFile'),
            preserve_default=True,
        ),
    ]
